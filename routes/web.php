<?php

use App\Models\ListRole;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
require __DIR__.'/auth.php';

Route::get('', 'HomeController@index');
Route::get('/', 'HomeController@index');
Route::resource('/home', 'Front\HomeController');

//register
Route::get('/pendaftaran', 'Front\RegisterController@index');
Route::post('/pendaftaran/store', 'Front\RegisterController@store');
Route::resource('/cek-pengaduan', 'Front\CekPengaduanController');
Route::post('/search-pengaduan', 'Front\CekPengaduanController@searchPengaduan')->name('searchpengaduan');


Route::namespace('Admin')->name('admin.')->prefix('admin')->middleware('auth', 'role:admin')->group(function() {
    Route::namespace('Master')->name('master.')->prefix('master')->group(function() {
        Route::get('', 'HomeController@index');
        Route::get('/dashboard', 'DashboardController@index')->name('dashboard.index');
        Route::resource('user', 'UserController');
        Route::get('data/user', 'UserController@data')->name('user.data');
        Route::resource('kategori-pengaduan', 'KategoriPengaduanController');
        Route::get('data/kategori-pengaduan', 'KategoriPengaduanController@data')->name('kategori-pengaduan.data');
        Route::resource('status-tindaklanjut', 'StatusTindaklanjutController');
        Route::get('data/status-tindaklanjut', 'StatusTindaklanjutController@data')->name('status-tindaklanjut.data');
        Route::resource('departemen', 'DepartemenController');
        Route::get('data/departemen', 'DepartemenController@data')->name('departemen.data');
        Route::resource('pengaduan', 'PengaduanController');
        Route::get('data/pengaduan', 'PengaduanController@data')->name('pengaduan.data');

        Route::resource('verifikasipengaduan', 'VerifikasiPengaduanController');
        Route::get('data/verifikasipengaduan', 'VerifikasiPengaduanController@data')->name('verifikasipengaduan.data');
    });
});

//pelapor
Route::namespace('Pelapor')->name('pelapor.')->prefix('pelapor')->middleware('auth', 'role:pelapor')->group(function() {
    Route::get('', 'HomeController@index');
    Route::resource('pengaduan', 'PengaduanController');
    Route::get('data/pengaduan', 'PengaduanController@data')->name('pengaduan.data');
    Route::get('pengaduan/getuserlogin', 'PengaduanController@getUserLogin');
    Route::get('/pengaduan/viewdetail/{id}', 'PengaduanController@viewDetail')->name('pengaduan.viewdetail'); 

    Route::resource('tindaklanjutpengaduan', 'TindakLanjutController');

    Route::resource('pengaduanditolak', 'PengaduanDitolakController');
    Route::resource('profile', 'ProfileController');
    Route::get('changepassword', 'ProfileController@viewChangePassword')->name('changepassword');
    Route::put('profile/changepassword/{id}', 'ProfileController@changePassword');
});

//departemen
Route::namespace('Departemen')->name('departemen.')->prefix('departemen')->middleware('auth', 'role:departemen')->group(function() {
    Route::get('', 'HomeController@index');
    Route::get('/dashboard', 'DashboardController@index')->name('dashboard.index');
    Route::resource('pengaduan', 'PengaduanController');
    Route::get('data/pengaduan', 'PengaduanController@data')->name('pengaduan.data');
    Route::get('/pengaduan/viewdetail/{id}', 'PengaduanController@viewDetail')->name('pengaduan.viewdetail'); 
    Route::post('/pengaduan/viewdetail/storetindaklanjut', 'PengaduanController@storetindaklanjut')->name('pengaduan.viewdetail.storetindaklanjut');
    Route::get('/pengaduan/viewdetail/edittindaklanjut/{id}', 'PengaduanController@edittindaklanjut')->name('pengaduan.viewdetail.edittindaklanjut');
    Route::put('/pengaduan/viewdetail/updatetindaklanjut/{id}', 'PengaduanController@updatetindaklanjut')->name('pengaduan.viewdetail.updatetindaklanjut');
    Route::put('/pengaduan/viewdetail/tutuppengaduan/{id}', 'PengaduanController@tutuppengaduan')->name('pengaduan.viewdetail.tutuppengaduan');
    Route::put('/pengaduan/viewdetail/bukapengaduan/{id}', 'PengaduanController@bukapengaduan')->name('pengaduan.viewdetail.bukapengaduan');
    Route::delete('/pengaduan/viewdetail/destroytindaklanjut/{id}', 'PengaduanController@destroytindaklanjut')->name('pengaduan.viewdetail.destroytindaklanjut');
});

//pengawas
Route::namespace('Pengawas')->name('pengawas.')->prefix('pengawas')->middleware('auth', 'role:pengawas')->group(function() {
    Route::get('', 'HomeController@index');
    Route::get('/dashboard', 'DashboardController@index')->name('dashboard.index');
    Route::resource('pengaduan', 'PengaduanController');
    Route::get('data/pengaduan', 'PengaduanController@data')->name('pengaduan.data');
    Route::get('/pengaduan/viewdetail/{id}', 'PengaduanController@viewDetail')->name('pengaduan.viewdetail'); 
    Route::post('/pengaduan/viewdetail/storetindaklanjut', 'PengaduanController@storetindaklanjut')->name('pengaduan.viewdetail.storetindaklanjut');
    Route::get('/pengaduan/viewdetail/edittindaklanjut/{id}', 'PengaduanController@edittindaklanjut')->name('pengaduan.viewdetail.edittindaklanjut');
    Route::put('/pengaduan/viewdetail/updatetindaklanjut/{id}', 'PengaduanController@updatetindaklanjut')->name('pengaduan.viewdetail.updatetindaklanjut');
    Route::put('/pengaduan/viewdetail/tutuppengaduan/{id}', 'PengaduanController@tutuppengaduan')->name('pengaduan.viewdetail.tutuppengaduan');
    Route::put('/pengaduan/viewdetail/bukapengaduan/{id}', 'PengaduanController@bukapengaduan')->name('pengaduan.viewdetail.bukapengaduan');
    Route::delete('/pengaduan/viewdetail/destroytindaklanjut/{id}', 'PengaduanController@destroytindaklanjut')->name('pengaduan.viewdetail.destroytindaklanjut');
});
