<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use App\Models\Role;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_pelapor = Role::where("name", "pelapor")->first();
        $role_admin  = Role::where("name", "admin")->first();
        $pelapor = new User();
        $pelapor->name = "Pelapor";
        $pelapor->email = "pelapor@arcom.id";
        $pelapor->password = Hash::make("4dminarc");
        $pelapor->save();
        $pelapor->roles()->attach($pelapor);

        $admin = new User();
        $admin->name = "Admin";
        $admin->email = "admin@arcom.id";
        $admin->password = Hash::make("4dminarcm");
        $admin->save();
        $admin->roles()->attach($role_admin);
    }
}
