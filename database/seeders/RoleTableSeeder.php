<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_employee = new Role();
        $role_employee->name = "admin";
        $role_employee->description = "Admin Sistem";
        $role_employee->save();
        $role_manager = new Role();
        $role_manager->name = "pelapor";
        $role_manager->description = "User Pelapor";
        $role_manager->save();
        $role_manager = new Role();
        $role_manager->name = "departemen";
        $role_manager->description = "Departemen";
        $role_manager->save();
        $role_manager = new Role();
        $role_manager->name = "pengawas";
        $role_manager->description = "Pengawas";
        $role_manager->save();
    }
}
