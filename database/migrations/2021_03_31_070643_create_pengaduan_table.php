<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePengaduanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pengaduan', function (Blueprint $table) {
            $table->increments('id');
            $table->date('peng_tgl');
            $table->integer('user_id');
            $table->string('peng_nama');
            $table->string('peng_hp');
            $table->string('peng_email');
            $table->integer('kp_id');
            $table->string('peng_topik');
            $table->text('peng_isi_pengaduan');
            $table->text('peng_file');
            $table->integer('peng_identitas_pelapor');
            $table->integer('peng_sifat_pengaduan');
            $table->integer('stl_id');
            $table->integer('dept_id');
            $table->integer('peng_verifikasi');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pengaduan');
    }
}
