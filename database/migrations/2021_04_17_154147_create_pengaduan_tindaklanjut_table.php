<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePengaduanTindaklanjutTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pengaduan_tindaklanjut', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('peng_id');
            $table->integer('user_id');
            $table->integer('stl_id');
            $table->text('pengt_tindaklanjut');
            $table->text('pengt_file');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pengaduan_tindaklanjut');
    }
}
