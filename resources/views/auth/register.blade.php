@extends('front.layouts.app')
@section('title', 'Register')
@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/extensions/toastr.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/extensions/ext-component-toastr.min.css') }}">
@endsection
@section('content')
        <!-- ======= Contact Section ======= -->
        <section id="pengaduan" class="contact section-bg" style="padding: 120px 0 60px 0">
            <div class="container" data-aos="fade-up">
    
            <div class="section-title offset-1">
                <h2>Pengaduan Online</h2>
                <p style="text-transform: initial;">Rs. Bhayangkara</p>
            </div>
                <div class="row">
                    <div class="col-lg-10 offset-1">
                        <form action="{{ url('/pendaftaran/store') }}" method="post" role="form" class="php-email-form" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" class="custom-control-input" value="2" name="role[]" id="check_2" />
                            <div class="complaint-form-box">
                                <div class="select-complaint">Pendaftaran User</div>
                            </div>
                            <div class="form-row">
                                <div class="col form-group">
                                <input type="text" class="form-control" id="name" name="name" placeholder="Nama Anda *" data-rule="minlen:4" data-msg="Please enter at least 4 chars" required/>
                                <div class="validate"></div>
                                </div>
                                <div class="col form-group">
                                <input type="email" class="form-control" id="email" name="email" placeholder="Email Anda *" data-rule="email" data-msg="Please enter a valid email" required/>
                                <div class="validate"></div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col form-group">
                                <input type="password" class="form-control" id="password" name="password" placeholder="Password *" required />
                                <div class="validate"></div>
                                </div>
                                <div class="col form-group">
                                <input type="password" class="form-control" id="password_confirmation" placeholder="Ulangi Password *" name="password_confirmation" required />
                                <div class="validate"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="no_hp" placeholder="No HP Anda" >
                                <div class="validate"></div>
                            </div>
                            <div class="form-group">
                                <textarea class="form-control" name="address" rows="5" placeholder="Masukkan Alamat Anda"></textarea>
                                <div class="validate"></div>
                            </div>
                            <div class="form-group">
                                <label for="classification_complaint" class="choose-classification">Photo Profile *</label>
                                <input type="file" style="padding:0px;" accept="image/*" name="user_profile" id="user_profile" required/>
                            </div>
                            <div class="text-center"><button type="submit">Daftar</button></div>
                        </form>
                    </div>
                </div>
            
            </div>
        </section><!-- End Contact Section -->  
@endsection
@section('js')
    <script src="{{ asset('app-assets/vendors/js/extensions/toastr.min.js') }}"></script>
    <script>
		// Modal
		$(document).ready(function() {
			$.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
			// Tambah/Ubah Data
			$('#form').on('submit', function(e) {
				e.preventDefault();

				var formData = new FormData(this);

				$.ajax({
					url: $(this).attr('action'),
					type: "POST",
					data: formData,
					contentType: false,
					processData: false
				}).done(function(response){
					$('#modal').modal('hide');
					formExecuted(response);
				});
			});
		});

        function formExecuted(response) {
			var status = '';
			if (response.id == 1) {
				status = 'success';
				location.href ='{{url('/login')}}';
			} else {
				status = 'error';
			}
			toastr[status](response.keterangan, response.status, {
				closeButton: true,
				tapToDismiss: true
			});
		}
	</script>
@endsection