@if (request()->is('admin') || request()->is('admin/*'))
    <li class="navigation-header"><span>Analytics</span><i data-feather="more-horizontal"></i></li>
    <li class="nav-item {{ (request()->routeIs('admin.master.dashboard.*')) ? 'active' : '' }}"><a class="d-flex align-items-center" href="{{route('admin.master.dashboard.index')}}"><i data-feather="home"></i><span class="menu-title text-truncate">Dashboard</span></a></li>
    
    {{-- Data Master--}}
    <li class="navigation-header"><span>Master</span><i data-feather="more-horizontal"></i></li>
    <li class="nav-item {{ (request()->routeIs('admin.master.user.*')) ? 'active' : '' }}"><a class="d-flex align-items-center" href="{{ route('admin.master.user.index') }}"><i data-feather="user"></i><span class="menu-title text-truncate">User</span></a></li>
    <li class="nav-item {{ (request()->routeIs('admin.master.kategori-pengaduan.*')) ? 'active' : '' }}"><a class="d-flex align-items-center" href="{{ route('admin.master.kategori-pengaduan.index') }}"><i data-feather="cloud"></i><span class="menu-title text-truncate">Kategori Pengaduan</span></a></li>
    <li class="nav-item {{ (request()->routeIs('admin.master.status-tindaklanjut.*')) ? 'active' : '' }}"><a class="d-flex align-items-center" href="{{ route('admin.master.status-tindaklanjut.index') }}"><i data-feather="activity"></i><span class="menu-title text-truncate">Status Tindak Lanjut</span></a></li>
    <li class="nav-item {{ (request()->routeIs('admin.master.departemen.*')) ? 'active' : '' }}"><a class="d-flex align-items-center" href="{{ route('admin.master.departemen.index') }}"><i data-feather="box"></i><span class="menu-title text-truncate">Departemen</span></a></li>
    
    <li class="navigation-header"><span>Manajemen Pengaduan</span><i data-feather="more-horizontal"></i></li>
    <li class="nav-item {{ (request()->routeIs('admin.master.pengaduan.*')) ? 'active' : '' }}"><a class="d-flex align-items-center" href="{{ route('admin.master.pengaduan.index') }}"><i data-feather="package"></i><span class="menu-title text-truncate">Pengaduan</span></a></li>
    <li class="nav-item {{ (request()->routeIs('admin.master.verifikasipengaduan.*')) ? 'active' : '' }}"><a class="d-flex align-items-center" href="{{ route('admin.master.verifikasipengaduan.index') }}"><i data-feather="check-circle"></i><span class="menu-title text-truncate">Verifikasi Pengaduan</span> <span class="badge mr-1 ml-auto badge-light-danger badge-pill">{{$total_pengaduan_bv ?? ''}}</span></a></li>

@elseif (request()->is('pelapor*'))
    <li class="navigation-header"><span>Manajemen Pengaduan</span><i data-feather="more-horizontal"></i></li>
    <li class="nav-item {{ (request()->routeIs('pelapor.pengaduan.*')) ? 'active' : '' }}"><a class="d-flex align-items-center" href="{{ route('pelapor.pengaduan.index') }}"><i data-feather="package"></i><span class="menu-title text-truncate">Pengaduan</span></a></li>
    <li class="navigation-header"><span>Setting</span><i data-feather="more-horizontal"></i></li>
    <li class="nav-item {{ (request()->routeIs('pelapor.profile.*')) ? 'active' : '' }}"><a class="d-flex align-items-center" href="{{ route('pelapor.profile.index') }}"><i data-feather="user"></i><span class="menu-title text-truncate">Profil</span></a></li>
    <li class="nav-item {{ (request()->routeIs('pelapor.changepassword')) ? 'active' : '' }}"><a class="d-flex align-items-center" href="{{ route('pelapor.changepassword') }}"><i data-feather="key"></i><span class="menu-title text-truncate">Ganti Password</span></a></li>
    
    
@elseif (request()->is('departemen*'))
    <li class="navigation-header"><span>Analytics</span><i data-feather="more-horizontal"></i></li>
    <li class="nav-item {{ (request()->routeIs('departemen.dashboard.*')) ? 'active' : '' }}"><a class="d-flex align-items-center" href="{{route('departemen.dashboard.index')}}"><i data-feather="home"></i><span class="menu-title text-truncate">Dashboard</span></a></li>
    <li class="navigation-header"><span>Manajemen Pengaduan</span><i data-feather="more-horizontal"></i></li>
    <li class="nav-item {{ (request()->routeIs('departemen.pengaduan.*')) ? 'active' : '' }}"><a class="d-flex align-items-center" href="{{ route('departemen.pengaduan.index') }}"><i data-feather="package"></i><span class="menu-title text-truncate">Pengaduan</span> <span class="badge mr-1 ml-auto badge-light-danger badge-pill">{{$total_btl ?? ''}}</span></a></li>

@elseif (request()->is('pengawas*'))
    <li class="navigation-header"><span>Analytics</span><i data-feather="more-horizontal"></i></li>
    <li class="nav-item {{ (request()->routeIs('pengawas.dashboard.*')) ? 'active' : '' }}"><a class="d-flex align-items-center" href="{{route('pengawas.dashboard.index')}}"><i data-feather="home"></i><span class="menu-title text-truncate">Dashboard</span></a></li>
    <li class="navigation-header"><span>Manajemen Pengaduan</span><i data-feather="more-horizontal"></i></li>
    <li class="nav-item {{ (request()->routeIs('pengawas.pengaduan.*')) ? 'active' : '' }}"><a class="d-flex align-items-center" href="{{ route('pengawas.pengaduan.index') }}"><i data-feather="package"></i><span class="menu-title text-truncate">Pengaduan</span></a></li>

@endif