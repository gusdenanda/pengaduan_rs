<div class="card">
    <div class="card-header border-bottom">
        <h4 class="card-title">{{ $title }}</h4>
    </div>
    {{ $slot }}
</div>