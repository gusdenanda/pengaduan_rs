@extends('layout.layout')

@section('title', 'Data Master User')

@section('css')
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/dataTables.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/buttons.bootstrap4.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/extensions/toastr.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/extensions/ext-component-toastr.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/extensions/sweetalert2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/extensions/ext-component-sweet-alerts.min.css') }}">
@endsection

@section('content')
	{{-- BEGIN: Datatable Button --}}
	<section>
		{{-- BEGIN: Table --}}
		<x-datatable-button title="Data User" id="tambah" buttonTitle='<i data-feather="plus"></i> Tambah'>
			<table class="table table-bordered">
				<thead>
					<tr>
						<th data-priority="1">Email</th>
						<th>Nama</th>
						<th>No. HP</th>
						<th>Role</th>
						<th data-priority="2" width="15%"></th>
					</tr>
				</thead>
				<tbody></tbody>
			</table>
		</x-datatable-button>
		{{-- END: Table --}}
		
		{{-- BEGIN: Modal --}}
		<x-modal title="" type="normal" class="" id="modal">
			<form id="form" data-value="">
				<div class="modal-body">
                    <x-form-group title="Email">
						<input type="email" class="form-control" id="email" placeholder="E-Mail User" required>
					</x-form-group>
					<x-form-group title="Nama">
						<input type="text" class="form-control" id="nama" placeholder="Nama User" required/>
					</x-form-group>
					<x-form-group title="No. HP">
						<input type="text" class="form-control" id="no_hp" placeholder="No. HP"/>
					</x-form-group>
					<x-form-group title="Role (Minimal isi 1)">
						<div class="custom-control custom-checkbox">
							<input type="checkbox" class="custom-control-input" value="1" name="role[]" id="check_1" />
							<label class="custom-control-label" for="check_1">Admin</label>
						</div>
				
						<div class="custom-control custom-checkbox">
							<input type="checkbox" class="custom-control-input" value="2" name="role[]" id="check_2" />
							<label class="custom-control-label" for="check_2">User Pelapor</label>
						</div>
				
						<div class="custom-control custom-checkbox">
							<input type="checkbox" class="custom-control-input" value="3" name="role[]" id="check_3" />
							<label class="custom-control-label" for="check_3">Departemen</label>
						</div>
				
						<div class="custom-control custom-checkbox">
							<input type="checkbox" class="custom-control-input" value="4" name="role[]" id="check_4" />
							<label class="custom-control-label" for="check_4">Pengawas</label>
						</div>
					</x-form-group>
					<x-form-group title="Departemen">
						<select class="form-control" id="dept_id" name="dept_id">
							<option value="" hidden>-- Pilih Departemen --</option>
							@foreach ($dept as $item)
								<option value="{{ $item->id }}">{{ $item->dept_nama }}</option>
							@endforeach
						</select>
					</x-form-group>
                    <x-form-group title="Password">
						<input type="password" class="form-control" id="password" placeholder="Password"/>
					</x-form-group>
                    <x-form-group title="Confirm Password">
						<input type="password" class="form-control" id="password_confirmation" placeholder="Confirm Password" />
					</x-form-group>
				</div>
			</form>

			<div class="modal-footer">
				<button type="submit" class="btn btn-primary" id="button-tambah" form="form"><i data-feather="check"></i> Simpan</button>
				<button type="submit" class="btn btn-primary" id="button-ubah" form="form"><i data-feather="edit-2"></i> Ubah</button>
				<button type="reset" class="btn btn-outline-danger" id="button-batal" data-dismiss="modal"><i data-feather="x"></i> Batal</button>
				<button type="button" class="btn btn-outline-danger" id="button-hapus"><i data-feather="trash"></i> Hapus</button>
			</div>
		</x-modal>
		{{-- END: Modal --}}
	</section>
	{{-- END: Datatable Button --}}
@endsection

@section('js')
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js') }}"></script>
	<script src="{{ asset('app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js') }}"></script>
	<script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.buttons.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/jszip.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/pdfmake.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/buttons.print.min.js') }}"></script>
	<script src="{{ asset('app-assets/vendors/js/extensions/toastr.min.js') }}"></script>
	<script src="{{ asset('app-assets/vendors/js/extensions/sweetalert2.all.min.js') }}"></script>

	{{-- Datatable --}}
	<script>
		var link = "{{ route('admin.master.user.data') }}";
		var column = [
			{data: 'email', name: 'email'},
			{data: 'name', name: 'name'},
			{data: 'no_hp', name: 'no_hp'},
			{data: 'role', name: 'role', orderable: false, searchable: false},
			{data: 'aksi', name: 'aksi', orderable: false, searchable: false}
		];
	</script>
	<script src="{{ asset('js/component/datatable-button.js') }}"></script>

    <script>
		// Modal
		$(document).ready(function() {
			$.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

			$('#tambah').on('click', function() {
				$('.modal-title').text('Tambah User');

				$('#form').attr("action", "{{ url('/admin/master/user') }}");
				$('#form').attr("data-value", "1");

				$('#button-tambah').attr('hidden', false);
				$('#button-batal').attr('hidden', false);
				$('#button-ubah').attr('hidden', true);
				$('#button-hapus').attr('hidden', true);

				$('#radio_1').prop('checked', true);
				
				$('#modal').modal('show');
			});

			$(document).on('click', '.ubah', function() {
				var id = $(this).attr('data-value');
				$('.modal-title').text('Ubah User');

				$('#form').attr("data-value", "2");

				$('#button-tambah').attr('hidden', true);
				$('#button-batal').attr('hidden', true);
				$('#button-ubah').attr('hidden', false);
				$('#button-hapus').attr('hidden', false);
				$('#button-verified').attr('hidden', true);

				$.get( "{{ url('admin/master/user') }}/"+ id, function( data ) {
                    var d = JSON.parse(data);
					$('#form').attr("action", "{{ url('/admin/master/user')}}/"+ id);
					$('#email').val(d.email);
					$('#nama').val(d.name);
					$('#no_hp').val(d.no_hp);
					$('#dept_id').val(d.dept_id);
					for (let i = 0; i < d.roles.length; i++) {
						$('#check_' + d.roles[i]['role_id']).prop('checked', true);
					}
					$('#button-hapus').attr('onClick', 'hapus('+ id +')');
                });

				$('#modal').modal('show');
			});

			$('#modal').on('hidden.bs.modal', function (e) {
				$('.modal-title').text('Menunggu...');
				$('#password').val('');
				$('#nama').val('');
				$('#no_hp').val('');
				$('#check_1').prop('checked', false);
				$('#check_2').prop('checked', false);
				$('#check_3').prop('checked', false);
				$('#check_4').prop('checked', false);
				$('#email').val('');
				$('#dept_id').val('');
			});

			// Tambah/Ubah Data
			$('#form').on('submit', function(e) {
				e.preventDefault();
				
				var roles = [];
				$("input[name='role[]']:checked").each(function () {
					roles.push($(this).val());
				});

				if ($("input[name='role[]']:checked").length > 0) {
					var status = '';
					if ($('#form').attr('data-value') == 1) {
						$.ajax({
							url: $(this).attr('action'),
							type: "POST",
							data: {
								name: $('#nama').val(),
								no_hp: $('#no_hp').val(),
								dept_id: $('#dept_id').val(),
								role: roles,
								email: $('#email').val(),
								password: $('#password').val(),
								password_confirmation: $('#password').val(),
						},
						}).done(function(response){
							formExecuted(response)
						});
					} else {
						$.ajax({
							url: $(this).attr('action'),
							type: "POST",
							data: {
								_method: 'PUT',
								name: $('#nama').val(),
								no_hp: $('#no_hp').val(),
								role: roles,
								email: $('#email').val(),
								dept_id: $('#dept_id').val(),
								password: $('#password').val(),
								password_confirmation: $('#password').val(),
						},
						}).done(function(response){
							formExecuted(response)
						});
					}
				} else {
					roleKosong();
				}
			});

			// Verifikasi Email
			$('#verifiedForm').on('submit', function(e) {
				e.preventDefault();

				var formData = new FormData(this);
				
				$.ajax({
					url: $(this).attr('action'),
					type: "POST",
					data: formData,
					contentType: false,
					processData: false
				}).done(function(response){
					formExecuted(response)
				});
			});

			$('form').submit(function() {
				$(this).find("button[type='submit']").prop('disabled', true);
			});
		});

		function roleKosong() {
			toastr['error']('Role wajib diisi minimal 1', 'Gagal', {
				closeButton: true,
				tapToDismiss: true
			});
		}

		function formExecuted(response) {
			if (response.id == 1) {
				status = 'success';
				table.ajax.reload();
				$('#modal').modal('hide');
			} else {
				status = 'error';
			}
			toastr[status](response.keterangan, response.status, {
				closeButton: true,
				tapToDismiss: true
			});

			$('form').find("button[type='submit']").prop('disabled', false);
		}

		// Hapus Data
		function hapus(id) {
			Swal.fire({
				title: 'Yakin ingin hapus?',
				icon: 'warning',
				showCancelButton: true,
				confirmButtonText: 'Ya',
				cancelButtonText: 'Tidak',
				customClass: {
					confirmButton: 'btn btn-primary',
					cancelButton: 'btn btn-outline-danger ml-1'
				},
				buttonsStyling: false
			}).then(function (result) {
				if (result.value) {
					var status = '';
					$.ajax({
						url: '{{ url("/admin/master/user")}}/'+ id,
						type: "POST",
						data: {
							_method: 'DELETE',
						},
					}).done(function(response){
						if (response.id == 1) {
							status = 'success';
							table.ajax.reload();
							$('#modal').modal('hide');
						} else {
							status = 'error';
						}
						toastr[status](response.keterangan, response.status, {
							closeButton: true,
							tapToDismiss: true
						});
					});
				}
			});
		}
	</script>
@endsection