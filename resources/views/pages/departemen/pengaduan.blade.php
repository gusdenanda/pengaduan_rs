@extends('layout.layout')

@section('title', 'Pengaduan')

@section('css')
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/dataTables.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/buttons.bootstrap4.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/extensions/toastr.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/extensions/ext-component-toastr.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/extensions/sweetalert2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/extensions/ext-component-sweet-alerts.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/pickers/flatpickr/flatpickr.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/forms/pickers/form-flat-pickr.min.css') }}">
@endsection

@section('content')
	{{-- BEGIN: Datatable Button --}}
	<section>
        {{-- BEGIN: Table --}}
        <x-datatable-button title="Data Pengaduan" id="" buttonTitle=''>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th data-priority="1">Tanggal</th>
						<th>Nama Pelapor</th>
						<th>Topik</th>
						<th>Tindak Lanjut</th>
						<th>Verifikasi</th>
						<th>Tindak Lanjut</th>
						<th data-priority="2">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    
                </tbody>
            </table>

            <form id="delete-form" action="#" method="POST" class="d-none">
                @csrf
                @method('DELETE')
            </form>
        </x-datatable-button>
        {{-- END: Table --}}
		{{-- END: Modal --}}
		<x-modal title="" type="normal" class="modal-lg" id="modal">
            <form name="form" class="form form-horizontal" id="form" method="post" enctype="multipart/form-data">
                <div class="modal-body">
                    <div class="form-group row">
                        <div class="col-sm-3 col-form-label">
                            <label for="first-name">Tanggal Pengaduan <span style="color:red">*</span></label>
                        </div>
                        <div class="col-sm-9">
                            <input type="text" class="form-control flatpickr-basic" name="peng_tgl" id="peng_tgl" placeholder="Tanggal Pengaduan" required/>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-3 col-form-label">
                            <label for="first-name">Nama Pelapor <span style="color:red">*</span></label>
                        </div>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="peng_nama" id="peng_nama" placeholder="Nama Pelapor" required/>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-3 col-form-label">
                            <label for="first-name">No Hp Pelapor</label>
                        </div>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="peng_hp" id="peng_hp" placeholder="No HP Pelapor" />
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-3 col-form-label">
                            <label for="first-name">Email Pelapor</label>
                        </div>
                        <div class="col-sm-9">
                            <input type="email" class="form-control" name="peng_email" id="peng_email" placeholder="Email Pelapor" />
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-3 col-form-label">
                            <label for="first-name">Identitas Pelapor <span style="color:red">*</span></label>
                        </div>
                        <div class="col-sm-9">
                            <select class="form-control" id="peng_identitas_pelapor" name="peng_identitas_pelapor" required>
                                <option value="1">Identitas tidak dirahasikan</option>
                                <option value="0">Identitas dirahasikan</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-3 col-form-label">
                            <label for="first-name">Kategori Pengaduan <span style="color:red">*</span></label>
                        </div>
                        <div class="col-sm-9">
                            <select class="form-control" id="kp_id" name="kp_id" required>
                                <option value="" hidden>-- Pilih Kategori Pengaduan --</option>
                                @foreach ($kp as $item)
                                    <option value="{{ $item->id }}">{{ $item->kp_nama }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-3 col-form-label">
                            <label for="first-name">Topik Pengaduan <span style="color:red">*</span></label>
                        </div>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="peng_topik" id="peng_topik" placeholder="Topik Pengaduan" required/>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-3 col-form-label">
                            <label for="first-name">Isi Pengaduan <span style="color:red">*</span></label>
                        </div>
                        <div class="col-sm-9">
                            <textarea class="form-control" name="peng_isi_pengaduan" id="peng_isi_pengaduan" placeholder="Isi Pengaduan Anda" rows="4" required></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-3 col-form-label">
                            <label for="first-name">Sifat Pengaduan <span style="color:red">*</span></label>
                        </div>
                        <div class="col-sm-9">
                            <select class="form-control" id="peng_sifat_pengaduan" name="peng_sifat_pengaduan" required>
                                <option value="1">Terbuka</option>
                                <option value="0">Rahasia</option>
                            </select>
                        </div>
					</div>
                    <div class="form-group row">
                        <div class="col-sm-3 col-form-label">
                            <label for="first-name">Departemen Yang Dituju <span style="color:red">*</span></label>
                        </div>
                        <div class="col-sm-9">
                            <select class="form-control" id="dept_id" name="dept_id" required>
                                <option value="" hidden>-- Pilih Kategori Pengaduan --</option>
                                @foreach ($dept as $item)
                                    <option value="{{ $item->id }}">{{ $item->dept_nama }}</option>
                                @endforeach
                            </select>
                        </div>
					</div>
                    <div class="form-group row">
                        <div class="col-sm-3 col-form-label">
                            <label for="first-name">File Pendukung</label>
                        </div>
                        <div class="col-sm-9">
                            <input type="file" class="form-control-file" accept="application/msword, application/vnd.ms-excel, application/vnd.ms-powerpoint,
                        text/plain, application/pdf, image/*" name="peng_file" id="peng_file"/>
                        </div>
					</div>
					<input type="hidden" name="stl_id" id="stl_id" value="2"/>
                    <input type="hidden" name="peng_verifikasi" id="peng_verifikasi" value="1"/>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary mr-1" id="button-tambah"><i data-feather="check"></i> Simpan</button>
                    <button type="submit" class="btn btn-primary mr-1" id="button-ubah"><i data-feather="edit-2"></i> Ubah</button>
                    <button type="reset" class="btn btn-outline-danger" id="button-batal" data-dismiss="modal"><i data-feather="x"></i> Batal</button>
                    <button type="button" class="btn btn-outline-danger" id="button-hapus"><i data-feather="trash"></i> Hapus</button>
                </div>
            </form>
		</x-modal>
	</section>
	{{-- END: Datatable Button --}}
@endsection

@section('js')
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js') }}"></script>
	<script src="{{ asset('app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js') }}"></script>
	<script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.buttons.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/jszip.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/pdfmake.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/buttons.html5.min.js') }}"></script>
	<script src="{{ asset('app-assets/vendors/js/tables/datatable/buttons.print.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/extensions/toastr.min.js') }}"></script>
	<script src="{{ asset('app-assets/vendors/js/pickers/flatpickr/flatpickr.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/extensions/sweetalert2.all.min.js') }}"></script>
    <script src="{{ asset('app-assets/js/scripts/extensions/ext-component-sweet-alerts.js') }}"></script>

	{{-- Datatable --}}
	<script>
		var link = null;
	</script>
    {{-- Datatable --}}
	<script>
		var link = "{{ route('departemen.pengaduan.data') }}";
		var column = [
			{data: 'peng_tgl', name: 'peng_tgl'},
			{data: 'peng_nama', name: 'peng_nama'},
			{data: 'peng_topik', name: 'peng_topik'},
			{data: 'stl_nama', name: 'stl_nama'},
            {data: 'peng_verifikasi', name: 'peng_verifikasi'},
            {data: 'tl', name: 'tl'},
			{data: 'aksi', name: 'aksi', orderable: false, searchable: false}
		];
	</script>
	<script src="{{ asset('js/component/datatable-button.js') }}"></script>
	<script>
		// Modal
		$(document).ready(function() {
			$.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $('.flatpickr-basic').flatpickr();
			$('#tambah').on('click', function() {
				$('.modal-title').text('Tambah Pengaduan');
                $('#form').attr('action', "{{ url('/departemen/pengaduan') }}");
				$('#button-tambah').attr('hidden', false);
				$('#button-batal').attr('hidden', false);
				$('#button-ubah').attr('hidden', true);
				$('#button-hapus').attr('hidden', true);
				
				$('#modal').modal('show');
			});

			$(document).on('click', '.ubah', function() {
				const id = $(this).attr('data-value');
				$('#button-hapus').attr('onClick', "hapus("+ $(this).attr('data-value') +")");
                $('#form').append('<input type="hidden" id="method" name="_method" value="PUT"/>');
				$.get( "{{ url('/departemen/pengaduan') }}/"+ id, function( data ) {
					var d = JSON.parse(data);
					$('.modal-title').text('Ubah Pengaduan');
					$('#form').attr('action', "{{ url('/departemen/pengaduan') }}/" + id);
					$('#peng_tgl').val(d.peng_tgl);
					$('#peng_nama').val(d.peng_nama);
					$('#peng_hp').val(d.peng_hp);
					$('#peng_email').val(d.peng_email);
					$('#peng_identitas_pelapor').val(d.peng_identitas_pelapor);
					$('#kp_id').val(d.kp_id);
					$('#peng_topik').val(d.peng_topik);
					$('#peng_isi_pengaduan').val(d.peng_isi_pengaduan);
					$('#peng_sifat_pengaduan').val(d.peng_sifat_pengaduan);
					$('#dept_id').val(d.dept_id);
					$('#stl_id').val(d.stl_id);
					$('#peng_verifikasi').val(d.peng_verifikasi);
				});

				$('#button-tambah').attr('hidden', true);
				$('#button-batal').attr('hidden', true);
				$('#button-ubah').attr('hidden', false);
				$('#button-hapus').attr('hidden', false);


				$('#modal').modal('show');
			});

			$('#modal').on('hidden.bs.modal', function (e) {
				$('#form').attr('action', '');
                $('#method').remove();
				document.getElementById("form").reset();
			});

			// Tambah/Ubah Data
			$('#form').on('submit', function(e) {
				e.preventDefault();

				var formData = new FormData(this);

				$.ajax({
					url: $(this).attr('action'),
					type: "POST",
					data: formData,
					contentType: false,
					processData: false
				}).done(function(response){
					$('#modal').modal('hide');
					formExecuted(response);
				});
			});
		});

        function formExecuted(response) {
			var status = '';
			if (response.id == 1) {
				status = 'success';
				table.ajax.reload();
			} else {
				status = 'error';
			}
			toastr[status](response.keterangan, response.status, {
				closeButton: true,
				tapToDismiss: true
			});
		}

		// Hapus Data
		function hapus(id) {
			Swal.fire({
				title: 'Yakin ingin hapus?',
				icon: 'warning',
				showCancelButton: true,
				confirmButtonText: 'Ya',
				cancelButtonText: 'Tidak',
				customClass: {
					confirmButton: 'btn btn-primary',
					cancelButton: 'btn btn-outline-danger ml-1'
				},
				buttonsStyling: false
			}).then(function (result) {
				if (result.value) {
					var status = '';
					$.ajax({
						url: '{{ url("/departemen/pengaduan")}}/'+ id,
						type: "POST",
						data: {
							_method: 'DELETE',
						},
					}).done(function(response){
						if (response.id == 1) {
							status = 'success';
							table.ajax.reload();
							$('#modal').modal('hide');
						} else {
							status = 'error';
						}
						toastr[status](response.keterangan, response.status, {
							closeButton: true,
							tapToDismiss: true
						});
					});
				}
			});
		}
	</script>
@endsection