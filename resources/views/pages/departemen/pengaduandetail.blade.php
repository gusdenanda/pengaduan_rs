@extends('layout.layout')

@section('title', 'Pengaduan RS Bhayangkara')

@section('css')
	<meta name="csrf-token" content="{{ csrf_token() }}">
	
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/core/menu/menu-types/vertical-menu.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/forms/form-quill-editor.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/extensions/ext-component-toastr.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/pages/app-email.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/extensions/toastr.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/extensions/ext-component-toastr.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/extensions/sweetalert2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/extensions/ext-component-sweet-alerts.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/pickers/flatpickr/flatpickr.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/forms/pickers/form-flat-pickr.min.css') }}">
@endsection

@section('content')
<div class="app-content content email-application" style="margin:0px; padding:0px;">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-area-wrapper">
        <div class="sidebar-left">
            <div class="sidebar">
                <div class="sidebar-content email-app-sidebar">
                    <div class="email-app-menu">
                        
                    </div>
                </div>

            </div>
        </div>
        <div class="content-right">
            <div class="content-wrapper">
                <div class="content-header row">
                </div>
                <div class="content-body">
                    <div class="body-content-overlay"></div>
                    <!-- Email list Area -->
                    
                    <!-- Detailed Email View -->
                    <div class="email-app-details show">
                        <!-- Detailed Email Header starts -->
                        <div class="email-detail-header">
                            <div class="email-header-left d-flex align-items-center">
                                <span class="go-back mr-1">
                                    <a href="{{url('/departemen/pengaduan')}}" title="Kembali"><i data-feather="chevron-left" class="font-medium-4"></i></a>
                                </span>
                                <h4 class="email-subject mb-0">Pengaduan dari <b>{{$pengaduan->peng_nama}}</b> </h4> 
                                @if ($pengaduan->peng_verifikasi==3)
                                    <button type="button" style="margin-left:10px;" class="btn btn-outline-danger round waves-effect btn-sm">Status Pengaduan Ditutup</button>
                                @else
                                    <button type="button" style="margin-left:10px;" class="btn btn-outline-success round waves-effect btn-sm">Status Pengaduan Masih Dibuka</button> 
                                @endif
                            </div>
                        </div>
                        <!-- Detailed Email Header ends -->

                        <!-- Detailed Email Content starts -->
                        <div class="email-scroll-area"><br>
                           <div class="row">
                                    <div class="col-12">
                                        <div class="card">
                                            <div class="card-header email-detail-head">
                                                <div class="user-details d-flex justify-content-between align-items-center flex-wrap">
                                                    <div class="avatar mr-75">
                                                        @if (Auth::user()->photo=="")
                                                            <img src="{{asset('files/blank_profile.png')}}" alt="profile image"  width="48" height="48">
                                                        @else
                                                            <img src="{{asset('files')}}/{{Auth::user()->photo}}" alt="profile image"  width="48" height="48">
                                                        @endif
                                                    </div>
                                                    <div class="mail-items">
                                                        <h5 class="mb-0">{{$pengaduan->peng_nama}}</h5>
                                                        <div class="email-info-dropup dropdown">
                                                            <span role="button" class="font-small-3 text-muted" id="card_top01" aria-haspopup="true" aria-expanded="false">
                                                                {{$pengaduan->peng_email}}
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="mail-meta-item d-flex align-items-center">
                                                    @if ($pengaduan->peng_verifikasi==1)
                                                        <button type="button" class="btn btn-outline-primary round waves-effect btn-sm tambah" data-value="{{$pengaduan->id}}" style="margin-right:10px;">Tindak Lanjuti</button>
                                                    @endif
                                                    @if ($pengaduan->peng_verifikasi==3)
                                                        <button type="button" class="btn btn-outline-danger round waves-effect btn-sm" onclick="buka('{{$pengaduan->id}}')" style="margin-right:10px;">Buka Laporan</button>
                                                    @else
                                                        <button type="button" class="btn btn-outline-danger round waves-effect btn-sm" onclick="tutup('{{$pengaduan->id}}')" style="margin-right:10px;">Tutup Laporan</button>
                                                    @endif
                                                    <small class="mail-date-time text-muted">{{date("F j, Y, g:i a",strtotime($pengaduan->peng_tgl))}}</small>
                                                </div>
                                            </div>
                                            <div class="card-body mail-message-wrapper pt-2">
                                                <div class="mail-message">
                                                    <p class="card-text"><b>{{$pengaduan->peng_topik}}</b></p>
                                                    <p class="card-text">
                                                        {{$pengaduan->peng_isi_pengaduan}}
                                                    </p>
                                                </div>
                                            </div>
                                            @if ($pengaduan->peng_file!="")
                                                <div class="card-footer">
                                                    <div class="mail-attachments">
                                                        <div class="d-flex align-items-center mb-1">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-paperclip font-medium-1 mr-50"><path d="M21.44 11.05l-9.19 9.19a6 6 0 0 1-8.49-8.49l9.19-9.19a4 4 0 0 1 5.66 5.66l-9.2 9.19a2 2 0 0 1-2.83-2.83l8.49-8.48"></path></svg>
                                                            <h5 class="font-weight-bolder text-body mb-0">File Pendukung Pengaduan</h5>
                                                        </div>
                                                        <div class="d-flex flex-column">
                                                            <a href="{{url('files')}}/{{$pengaduan->peng_file}}" target="_blank" class="mb-50">
                                                                <img src="{{asset('app-assets/images/icons/doc.png')}}" class="mr-25" alt="png" height="18">
                                                                <small class="text-muted font-weight-bolder">{{$pengaduan->peng_file}}</small>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                @foreach ($tindaklanjut as $tl)
                                    <div class="col-11" style="margin-left: 60px;">
                                        <div class="card">
                                            <div class="card-header email-detail-head">
                                                <div class="user-details d-flex justify-content-between align-items-center flex-wrap">
                                                    <div class="avatar mr-75">
                                                        @if ($tl->photo=="")
                                                            <img src="{{asset('files/blank_profile.png')}}" alt="profile image"  width="48" height="48">
                                                        @else
                                                            <img src="{{asset('files')}}/{{$tl->photo}}" alt="profile image"  width="48" height="48">
                                                        @endif
                                                    </div>
                                                    <div class="mail-items">
                                                        <h5 class="mb-0">Tindak Lanjut dari {{$tl->name}}</h5>
                                                        <div class="email-info-dropup dropdown">
                                                            <span role="button" class="font-small-3 text-muted" id="card_top01" aria-haspopup="true" aria-expanded="false">
                                                                {{$tl->email}}
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="mail-meta-item d-flex align-items-center">
                                                    @if (Auth::user()->id==$tl->user_id && $pengaduan->peng_verifikasi==1)
                                                        <button type="button" class="btn btn-outline-success round waves-effect btn-sm ubah" data-value="{{$tl->id}}" style="margin-right:10px;">Edit</button>
                                                        <button type="button" class="btn btn-outline-danger round waves-effect btn-sm hapus" onclick="hapus('{{$tl->id}}','{{$tl->peng_id}}')" style="margin-right:10px;">Hapus</button>
                                                    @endif
                                                    <small class="mail-date-time text-muted">{{date("F j, Y, g:i a",strtotime($tl->created_at))}}</small>
                                                </div>
                                            </div>
                                            <div class="card-body mail-message-wrapper pt-2">
                                                <div class="mail-message">
                                                    <p class="card-text">
                                                        {{$tl->pengt_tindaklanjut}}
                                                    </p>
                                                </div>
                                            </div>
                                            @if ($tl->pengt_file!="")
                                                <div class="card-footer">
                                                    <div class="mail-attachments">
                                                        <div class="d-flex align-items-center mb-1">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-paperclip font-medium-1 mr-50"><path d="M21.44 11.05l-9.19 9.19a6 6 0 0 1-8.49-8.49l9.19-9.19a4 4 0 0 1 5.66 5.66l-9.2 9.19a2 2 0 0 1-2.83-2.83l8.49-8.48"></path></svg>
                                                            <h5 class="font-weight-bolder text-body mb-0">File Tindak Lanjut</h5>
                                                        </div>
                                                        <div class="d-flex flex-column">
                                                            <a href="{{url('files')}}/{{$tl->pengt_file}}" target="_blank" class="mb-50">
                                                                <img src="{{asset('app-assets/images/icons/doc.png')}}" class="mr-25" alt="png" height="18">
                                                                <small class="text-muted font-weight-bolder">{{$tl->pengt_file}}</small>
                                                            </a>
                                                        </div> 
                                                    </div>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <!-- Detailed Email Content ends -->
                    </div>
                    <x-modal title="" type="normal" class="modal-lg" id="modal">
                        <form name="form" class="form form-horizontal" id="form" method="post" enctype="multipart/form-data">
                            <input type="hidden" id="peng_id" name="peng_id" value="{{$pengaduan->id}}">
                            <div class="modal-body">
                                <div class="form-group row">
                                    <div class="col-sm-3 col-form-label">
                                        <label for="first-name">Status Tindak Lanjut <span style="color:red">*</span></label>
                                    </div>
                                    <div class="col-sm-9">
                                        <select class="form-control" id="stl_id" name="stl_id" required>
                                            <option value="" hidden>-- Pilih Status Tindak Lanjut --</option>
                                            @foreach ($stl as $item)
                                                <option value="{{ $item->id }}">{{ $item->stl_nama }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-3 col-form-label">
                                        <label for="first-name">Tindak Lanjut <span style="color:red">*</span></label>
                                    </div>
                                    <div class="col-sm-9">
                                        <textarea class="form-control" name="pengt_tindaklanjut" id="pengt_tindaklanjut" placeholder="Tindak Lanjut" rows="4" required></textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-3 col-form-label">
                                        <label for="first-name">File Pendukung</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <input type="file" class="form-control-file" accept="application/msword, application/vnd.ms-excel, application/vnd.ms-powerpoint,
                                    text/plain, application/pdf, image/*" name="pengt_file" id="pengt_file"/>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-primary mr-1" id="button-tambah"><i data-feather="check"></i> Simpan</button>
                                <button type="submit" class="btn btn-primary mr-1" id="button-ubah"><i data-feather="edit-2"></i> Ubah</button>
                                <button type="reset" class="btn btn-outline-danger" id="button-batal" data-dismiss="modal"><i data-feather="x"></i> Batal</button>
                                <button type="button" class="btn btn-outline-danger" id="button-hapus"><i data-feather="trash"></i> Hapus</button>
                            </div>
                        </form>
                    </x-modal>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
    <script src="{{ asset('app-assets/vendors/js/editors/quill/katex.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/editors/quill/highlight.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/editors/quill/quill.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/extensions/toastr.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/forms/select/select2.full.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/pickers/flatpickr/flatpickr.min.js') }}"></script>
	<script src="{{ asset('app-assets/vendors/js/pickers/flatpickr/flatpickr.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/extensions/sweetalert2.all.min.js') }}"></script>
    <script src="{{ asset('app-assets/js/scripts/extensions/ext-component-sweet-alerts.js') }}"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="{{ asset('app-assets/js/core/app-menu.js') }}"></script>
    <script src="{{ asset('app-assets/js/core/app.js') }}"></script>

    <script src="{{ asset('app-assets/js/scripts/pages/app-email.js') }}"></script>
	<script>
		// Modal
		$(document).ready(function() {
			$.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

			$('.tambah').on('click', function() {
                $('.modal-title').text('Tambah Tindak Lanjut');
                $('#form').attr('action', "{{ url('/departemen/pengaduan/viewdetail/storetindaklanjut') }}");
				$('#button-tambah').attr('hidden', false);
				$('#button-batal').attr('hidden', false);
				$('#button-ubah').attr('hidden', true);
				$('#button-hapus').attr('hidden', true);
                $('#modal').modal('show');
			});
            $('.flatpickr-basic').flatpickr();
			$(document).on('click', '.ubah', function() {
				const id = $(this).attr('data-value');
				$('#button-hapus').attr('onClick', "hapus("+ $(this).attr('data-value') +")");
                $('#form').append('<input type="hidden" id="method" name="_method" value="PUT"/>');
				$.get( "{{ url('/departemen/pengaduan/viewdetail/edittindaklanjut') }}/"+ id, function( data ) {
					var d = JSON.parse(data);
					$('.modal-title').text('Ubah Pengaduan');
					$('#form').attr('action', "{{ url('/departemen/pengaduan/viewdetail/updatetindaklanjut') }}/" + id);
					$('#pengt_tindaklanjut').val(d.pengt_tindaklanjut);
					$('#stl_id').val(d.stl_id);
				});

				$('#button-tambah').attr('hidden', true);
				$('#button-batal').attr('hidden', true);
				$('#button-ubah').attr('hidden', false);
				$('#button-hapus').attr('hidden', false);


				$('#modal').modal('show');
			});

			$('#modal').on('hidden.bs.modal', function (e) {
				$('#form').attr('action', '');
                $('#method').remove();
				$('#form')[0].reset();
			});

			// Tambah/Ubah Data
			$('#form').on('submit', function(e) {
				e.preventDefault();

				var formData = new FormData(this);

				$.ajax({
					url: $(this).attr('action'),
					type: "POST",
					data: formData,
					contentType: false,
					processData: false
				}).done(function(response){
					$('#modal').modal('hide');
					var peng_id = $('#peng_id').val();
					formExecuted(response,peng_id);
				});
			});
		});

        function formExecuted(response,peng_id) {
			var status = '';
			if (response.id == 1) {
				status = 'success';
				location.href ='{{url('/departemen/pengaduan/viewdetail')}}/'+peng_id;
			} else {
				status = 'error';
			}
			toastr[status](response.keterangan, response.status, {
				closeButton: true,
				tapToDismiss: true
			});
		}

		// Hapus Data
		function hapus(id,tl_id) {
			Swal.fire({
				title: 'Yakin ingin hapus?',
				icon: 'warning',
				showCancelButton: true,
				confirmButtonText: 'Ya',
				cancelButtonText: 'Tidak',
				customClass: {
					confirmButton: 'btn btn-primary',
					cancelButton: 'btn btn-outline-danger ml-1'
				},
				buttonsStyling: false
			}).then(function (result) {
				if (result.value) {
					var status = '';
					$.ajax({
						url: '{{ url("/departemen/pengaduan/viewdetail/destroytindaklanjut")}}/'+ id,
						type: "POST",
						data: {
							_method: 'DELETE',
						},
					}).done(function(response){
						if (response.id == 1) {
							status = 'success';
							location.href ='{{url('/departemen/pengaduan/viewdetail')}}/'+ tl_id;
							$('#modal').modal('hide');
						} else {
							status = 'error';
						}
						toastr[status](response.keterangan, response.status, {
							closeButton: true,
							tapToDismiss: true
						});
					});
				}
			});
		}
        function tutup(id) {
			Swal.fire({
				title: 'Yakin ingin Tutup Laporan?',
				icon: 'warning',
				showCancelButton: true,
				confirmButtonText: 'Ya',
				cancelButtonText: 'Tidak',
				customClass: {
					confirmButton: 'btn btn-primary',
					cancelButton: 'btn btn-outline-danger ml-1'
				},
				buttonsStyling: false
			}).then(function (result) {
				if (result.value) {
					var status = '';
					$.ajax({
						url: '{{ url("/departemen/pengaduan/viewdetail/tutuppengaduan")}}/'+ id,
						type: "POST",
						data: {
							_method: 'PUT',
						},
					}).done(function(response){
						if (response.id == 1) {
							status = 'success';
							location.href ='{{url('/departemen/pengaduan/viewdetail')}}/'+ id;
							$('#modal').modal('hide');
						} else {
							status = 'error';
						}
						toastr[status](response.keterangan, response.status, {
							closeButton: true,
							tapToDismiss: true
						});
					});
				}
			});
		}
        function buka(id) {
			Swal.fire({
				title: 'Yakin ingin membuka kembali Laporannya?',
				icon: 'warning',
				showCancelButton: true,
				confirmButtonText: 'Ya',
				cancelButtonText: 'Tidak',
				customClass: {
					confirmButton: 'btn btn-primary',
					cancelButton: 'btn btn-outline-danger ml-1'
				},
				buttonsStyling: false
			}).then(function (result) {
				if (result.value) {
					var status = '';
					$.ajax({
						url: '{{ url("/departemen/pengaduan/viewdetail/bukapengaduan")}}/'+ id,
						type: "POST",
						data: {
							_method: 'PUT',
						},
					}).done(function(response){
						if (response.id == 1) {
							status = 'success';
							location.href ='{{url('/departemen/pengaduan/viewdetail')}}/'+ id;
							$('#modal').modal('hide');
						} else {
							status = 'error';
						}
						toastr[status](response.keterangan, response.status, {
							closeButton: true,
							tapToDismiss: true
						});
					});
				}
			});
		}
	</script>
@endsection