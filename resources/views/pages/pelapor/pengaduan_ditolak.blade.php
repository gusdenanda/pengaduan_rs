@extends('layout.layout')

@section('title', 'Pengaduan RS Bhayangkara')

@section('css')
	<meta name="csrf-token" content="{{ csrf_token() }}">
	
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/core/menu/menu-types/vertical-menu.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/forms/form-quill-editor.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/extensions/ext-component-toastr.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/pages/app-email.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/extensions/toastr.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/extensions/ext-component-toastr.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/extensions/sweetalert2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/extensions/ext-component-sweet-alerts.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/pickers/flatpickr/flatpickr.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/forms/pickers/form-flat-pickr.min.css') }}">
@endsection

@section('content')
<div class="app-content content email-application" style="margin:0px; padding:0px;">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-area-wrapper">
        <div class="sidebar-left">
            <div class="sidebar">
                <div class="sidebar-content email-app-sidebar">
                    <div class="email-app-menu">
                        <div class="form-group-compose text-center compose-btn">
                            <button type="button" class="btn btn-primary btn-block" id="tambah">
                                Buat Pengaduan
                            </button>
                        </div>
                        <div class="sidebar-menu-list">
                            <div class="list-group list-group-messages">
                                <a href="{{url('/pelapor/pengaduan')}}" class="list-group-item list-group-item-action">
                                    <i data-feather="mail" class="font-medium-3 mr-50"></i>
                                    <span class="align-middle">Daftar Pengaduan</span>
                                    <span class="badge badge-light-primary badge-pill float-right">{{$count_pengaduan}}</span>
                                </a>
                                <a href="{{url('/pelapor/tindaklanjutpengaduan')}}" class="list-group-item list-group-item-action">
                                    <i data-feather="check-square" class="font-medium-3 mr-50"></i>
                                    <span class="align-middle">Pengaduan yang ditindaklanjuti</span>
                                    <span class="badge badge-light-success badge-pill float-right">{{$count_pengaduan_stl}}</span>
                                </a>
                                <a href="javascript:void(0)" class="list-group-item list-group-item-action active">
                                    <i data-feather="x-octagon" class="font-medium-3 mr-50"></i>
                                    <span class="align-middle">Pengaduan ditolak</span>
                                    <span class="badge badge-light-danger badge-pill float-right">{{$count_pengaduan_ditolak}}</span>
                                </a>
                            </div>
                            <!-- <hr /> -->
                            <h6 class="section-label mt-3 mb-1 px-2">Tautan Pintas</h6>
                            <div class="list-group list-group-labels">
                                <a href="{{url('/pelapor/profile')}}" class="list-group-item list-group-item-action"><span class="bullet bullet-sm bullet-success mr-1"></span>Update Profile</a>
                                <a href="{{route('pelapor.profile.changepassword')}}" class="list-group-item list-group-item-action"><span class="bullet bullet-sm bullet-primary mr-1"></span>Change Password</a>
                                <a class="list-group-item list-group-item-action" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                    <span class="bullet bullet-sm bullet-danger mr-1"></span>Logout
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                    @csrf
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="content-right">
            <div class="content-wrapper">
                <div class="content-header row">
                </div>
                <div class="content-body">
                    <div class="body-content-overlay"></div>
                    <!-- Email list Area -->
                    
                    <!-- Detailed Email View -->
                    <div class="email-app-details show">
                        <!-- Detailed Email Header starts -->
                        <div class="email-detail-header">
                            <div class="email-header-left d-flex align-items-center">
                                <span class="go-back mr-1"><i data-feather="package" class="font-medium-4"></i></span>
                                <h4 class="email-subject mb-0">Daftar Pengaduan yang Ditolak</h4>
                            </div>
                            <div class="email-header-right ml-2 pl-1">
                                <ul class="list-inline m-0">
                                    <li class="list-inline-item email-prev">
                                        <span class="action-icon"><i data-feather="chevron-left" class="font-medium-2"></i></span>
                                    </li>
                                    <li class="list-inline-item email-next">
                                        <span class="action-icon"><i data-feather="chevron-right" class="font-medium-2"></i></span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <!-- Detailed Email Header ends -->

                        <!-- Detailed Email Content starts -->
                        <div class="email-scroll-area"><br>
                            @foreach ($pengaduan as $data)
                                <div class="row">
                                    <div class="col-12">
                                        <div class="card">
                                            <div class="card-header email-detail-head">
                                                <div class="user-details d-flex justify-content-between align-items-center flex-wrap">
                                                    <div class="avatar mr-75">
                                                        @if (Auth::user()->name=="")
                                                            <img src="{{asset('files/blank_profile.png')}}" alt="profile image"  width="48" height="48">
                                                        @else
                                                            <img src="{{asset('files')}}/{{Auth::user()->photo}}" alt="profile image"  width="48" height="48">
                                                        @endif
                                                    </div>
                                                    <div class="mail-items">
                                                        <h5 class="mb-0">{{$data->peng_nama}}</h5>
                                                        <div class="email-info-dropup dropdown">
                                                            <span role="button" class="font-small-3 text-muted" id="card_top01" aria-haspopup="true" aria-expanded="false">
                                                                {{$data->peng_email}}
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="mail-meta-item d-flex align-items-center">
                                                    @if ($data->peng_verifikasi=="0")
                                                        <button type="button" class="btn btn-outline-success round waves-effect btn-sm ubah" data-value="{{$data->id}}" style="margin-right:10px;">Edit</button>
                                                        <button type="button" class="btn btn-outline-danger round waves-effect btn-sm hapus" onclick="hapus('{{$data->id}}')" style="margin-right:10px;">Hapus</button>
                                                    @endif
                                                    <small class="mail-date-time text-muted">{{date("F j, Y, g:i a",strtotime($data->peng_tgl))}}</small>
                                                    <!--<div class="dropdown ml-50">
                                                        <div role="button" class="dropdown-toggle hide-arrow" id="email_more" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            <i data-feather="more-vertical" class="font-medium-2"></i>
                                                        </div>
                                                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="email_more">
                                                            <div class="dropdown-item"><i data-feather="corner-up-left" class="mr-50"></i>Reply</div>
                                                            <div class="dropdown-item"><i data-feather="corner-up-right" class="mr-50"></i>Forward</div>
                                                            <div class="dropdown-item"><i data-feather="trash-2" class="mr-50"></i>Delete</div>
                                                        </div>
                                                    </div>-->
                                                </div>
                                            </div>
                                            <div class="card-body mail-message-wrapper pt-2">
                                                <div class="mail-message">
                                                    <p class="card-text"><b>{{$data->peng_topik}}</b></p>
                                                    <p class="card-text">
                                                        {{$data->peng_isi_pengaduan}}
                                                    </p>
                                                </div>
                                            </div>
                                            @if ($data->peng_file!="")
                                                <div class="card-footer">
                                                    <div class="mail-attachments">
                                                        <div class="d-flex align-items-center mb-1">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-paperclip font-medium-1 mr-50"><path d="M21.44 11.05l-9.19 9.19a6 6 0 0 1-8.49-8.49l9.19-9.19a4 4 0 0 1 5.66 5.66l-9.2 9.19a2 2 0 0 1-2.83-2.83l8.49-8.48"></path></svg>
                                                            <h5 class="font-weight-bolder text-body mb-0">File Pendukung</h5>
                                                        </div>
                                                        <div class="d-flex flex-column">
                                                            <a href="{{url('files')}}/{{$data->peng_file}}" target="_blank" class="mb-50">
                                                                <img src="{{asset('app-assets/images/icons/doc.png')}}" class="mr-25" alt="png" height="18">
                                                                <small class="text-muted font-weight-bolder">{{$data->peng_file}}</small>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <!-- Detailed Email Content ends -->
                    </div>
                    <x-modal title="" type="normal" class="modal-lg" id="modal">
                        <form name="form" class="form form-horizontal" id="form" method="post" enctype="multipart/form-data">
                            <div class="modal-body">
                                <div class="form-group row">
                                    <div class="col-sm-3 col-form-label">
                                        <label for="first-name">Tanggal Pengaduan <span style="color:red">*</span></label>
                                    </div>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control flatpickr-basic" name="peng_tgl" id="peng_tgl" placeholder="Tanggal Pengaduan" required/>
                                    </div>
                                </div>
                                <input type="hidden" class="form-control" name="peng_nama" id="peng_nama" placeholder="Nama Pelapor"/>
                                <input type="hidden" class="form-control" name="peng_hp" id="peng_hp" placeholder="No HP Pelapor" />
                                <input type="hidden" class="form-control" name="peng_email" id="peng_email" placeholder="Email Pelapor" />
                                <div class="form-group row">
                                    <div class="col-sm-3 col-form-label">
                                        <label for="first-name">Identitas Pelapor <span style="color:red">*</span></label>
                                    </div>
                                    <div class="col-sm-9">
                                        <select class="form-control" id="peng_identitas_pelapor" name="peng_identitas_pelapor" required>
                                            <option value="1">Identitas tidak dirahasikan</option>
                                            <option value="0">Identitas dirahasikan</option>
                                        </select> 
                                        <div class="alert alert-danger mt-1 alert-validation-msg" role="alert">
                                            <div class="alert-body">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-info mr-50 align-middle"><circle cx="12" cy="12" r="10"></circle><line x1="12" y1="16" x2="12" y2="12"></line><line x1="12" y1="8" x2="12.01" y2="8"></line></svg>
                                                <span>Jika identitas dirahasikan maka Nama, Email dan No Hp akan diganti secara otomatis menjadi Anonim</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-3 col-form-label">
                                        <label for="first-name">Kategori Pengaduan <span style="color:red">*</span></label>
                                    </div>
                                    <div class="col-sm-9">
                                        <select class="form-control" id="kp_id" name="kp_id" required>
                                            <option value="" hidden>-- Pilih Kategori Pengaduan --</option>
                                            @foreach ($kp as $item)
                                                <option value="{{ $item->id }}">{{ $item->kp_nama }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-3 col-form-label">
                                        <label for="first-name">Topik Pengaduan <span style="color:red">*</span></label>
                                    </div>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="peng_topik" id="peng_topik" placeholder="Topik Pengaduan" required/>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-3 col-form-label">
                                        <label for="first-name">Isi Pengaduan <span style="color:red">*</span></label>
                                    </div>
                                    <div class="col-sm-9">
                                        <textarea class="form-control" name="peng_isi_pengaduan" id="peng_isi_pengaduan" placeholder="Isi Pengaduan Anda" rows="4" required></textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-3 col-form-label">
                                        <label for="first-name">Sifat Pengaduan <span style="color:red">*</span></label>
                                    </div>
                                    <div class="col-sm-9">
                                        <select class="form-control" id="peng_sifat_pengaduan" name="peng_sifat_pengaduan" required>
                                            <option value="1">Terbuka</option>
                                            <option value="0">Rahasia</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-3 col-form-label">
                                        <label for="first-name">Departemen Yang Dituju <span style="color:red">*</span></label>
                                    </div>
                                    <div class="col-sm-9">
                                        <select class="form-control" id="dept_id" name="dept_id" required>
                                            <option value="" hidden>-- Pilih Kategori Pengaduan --</option>
                                            @foreach ($dept as $item)
                                                <option value="{{ $item->id }}">{{ $item->dept_nama }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-3 col-form-label">
                                        <label for="first-name">File Pendukung</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <input type="file" class="form-control-file" accept="application/msword, application/vnd.ms-excel, application/vnd.ms-powerpoint,
                                    text/plain, application/pdf, image/*" name="peng_file" id="peng_file"/>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-primary mr-1" id="button-tambah"><i data-feather="check"></i> Simpan</button>
                                <button type="submit" class="btn btn-primary mr-1" id="button-ubah"><i data-feather="edit-2"></i> Ubah</button>
                                <button type="reset" class="btn btn-outline-danger" id="button-batal" data-dismiss="modal"><i data-feather="x"></i> Batal</button>
                                <button type="button" class="btn btn-outline-danger" id="button-hapus"><i data-feather="trash"></i> Hapus</button>
                            </div>
                        </form>
                    </x-modal>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
    <script src="{{ asset('app-assets/vendors/js/editors/quill/katex.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/editors/quill/highlight.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/editors/quill/quill.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/extensions/toastr.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/forms/select/select2.full.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/pickers/flatpickr/flatpickr.min.js') }}"></script>
	<script src="{{ asset('app-assets/vendors/js/pickers/flatpickr/flatpickr.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/extensions/sweetalert2.all.min.js') }}"></script>
    <script src="{{ asset('app-assets/js/scripts/extensions/ext-component-sweet-alerts.js') }}"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="{{ asset('app-assets/js/core/app-menu.js') }}"></script>
    <script src="{{ asset('app-assets/js/core/app.js') }}"></script>

    <script src="{{ asset('app-assets/js/scripts/pages/app-email.js') }}"></script>
	<script>
		// Modal
		$(document).ready(function() {
			$.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

			$('#tambah').on('click', function() {
				$('.modal-title').text('Tambah Kategori Pengaduan');
                $('#form').attr('action', "{{ url('/pelapor/pengaduan') }}");
				$('#button-tambah').attr('hidden', false);
				$('#button-batal').attr('hidden', false);
				$('#button-ubah').attr('hidden', true);
				$('#button-hapus').attr('hidden', true);
				$('#peng_tgl').val('{{ date("Y-m-d")}}');
				$('#peng_nama').val('{{ Auth::user()->name }}');
				$('#peng_hp').val('{{ Auth::user()->no_hp}}');
				$('#peng_email').val('{{ Auth::user()->email }}');
				$('#modal').modal('show');
			});
            $('.flatpickr-basic').flatpickr();
			$(document).on('click', '.ubah', function() {
				const id = $(this).attr('data-value');
				$('#button-hapus').attr('onClick', "hapus("+ $(this).attr('data-value') +")");
                $('#form').append('<input type="hidden" id="method" name="_method" value="PUT"/>');
				$.get( "{{ url('/pelapor/pengaduan') }}/"+ id, function( data ) {
					var d = JSON.parse(data);
					$('.modal-title').text('Ubah Pengaduan');
					$('#form').attr('action', "{{ url('/pelapor/pengaduan') }}/" + id);
					$('#peng_tgl').val(d.peng_tgl);
					$('#peng_nama').val(d.peng_nama);
					$('#peng_hp').val(d.peng_hp);
					$('#peng_email').val(d.peng_email);
					$('#peng_identitas_pelapor').val(d.peng_identitas_pelapor);
					$('#kp_id').val(d.kp_id);
					$('#peng_topik').val(d.peng_topik);
					$('#peng_isi_pengaduan').val(d.peng_isi_pengaduan);
					$('#peng_sifat_pengaduan').val(d.peng_sifat_pengaduan);
					$('#dept_id').val(d.dept_id);
					$('#stl_id').val(d.stl_id);
					$('#peng_verifikasi').val(d.peng_verifikasi);
				});

				$('#button-tambah').attr('hidden', true);
				$('#button-batal').attr('hidden', true);
				$('#button-ubah').attr('hidden', false);
				$('#button-hapus').attr('hidden', false);


				$('#modal').modal('show');
			});

			$('#modal').on('hidden.bs.modal', function (e) {
				$('#form').attr('action', '');
                $('#method').remove();
				$('#form')[0].reset();
			});

			// Tambah/Ubah Data
			$('#form').on('submit', function(e) {
				e.preventDefault();

				var formData = new FormData(this);

				$.ajax({
					url: $(this).attr('action'),
					type: "POST",
					data: formData,
					contentType: false,
					processData: false
				}).done(function(response){
					$('#modal').modal('hide');
					formExecuted(response);
				});
			});
		});

        function formExecuted(response) {
			var status = '';
			if (response.id == 1) {
				status = 'success';
				location.href ='{{url('/pelapor/pengaduan')}}';
			} else {
				status = 'error';
			}
			toastr[status](response.keterangan, response.status, {
				closeButton: true,
				tapToDismiss: true
			});
		}

		// Hapus Data
		function hapus(id) {
			Swal.fire({
				title: 'Yakin ingin hapus?',
				icon: 'warning',
				showCancelButton: true,
				confirmButtonText: 'Ya',
				cancelButtonText: 'Tidak',
				customClass: {
					confirmButton: 'btn btn-primary',
					cancelButton: 'btn btn-outline-danger ml-1'
				},
				buttonsStyling: false
			}).then(function (result) {
				if (result.value) {
					var status = '';
					$.ajax({
						url: '{{ url("/pelapor/pengaduan")}}/'+ id,
						type: "POST",
						data: {
							_method: 'DELETE',
						},
					}).done(function(response){
						if (response.id == 1) {
							status = 'success';
							location.href ='{{url('/pelapor/pengaduan')}}';
							$('#modal').modal('hide');
						} else {
							status = 'error';
						}
						toastr[status](response.keterangan, response.status, {
							closeButton: true,
							tapToDismiss: true
						});
					});
				}
			});
		}
	</script>
@endsection