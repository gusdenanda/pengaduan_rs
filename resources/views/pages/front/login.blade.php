@extends('front.layouts.app')
@section('title', 'Login')
@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection
@section('content')
    <section id="cta" class="cta" style="padding: 140px 0 60px 0">
            <div class="container aos-init aos-animate" data-aos="zoom-in">
            <div class="text-center">
                <h3>Layanan Aspirasi dan Pengaduan Online</h3> <br>
                <p>Pengaduan Online rumah Sakit Bhayangkara merupakan sebuah platform yang diperuntukkan kepada Masyarakat Umum / Tenaga Medis / Pegawai Rumah Sakit Bhayangkara Denpasar untuk menyampaikan Aspirasi dan  Pengaduan.</p><br>
                <a class="cta-btn" href="#pengaduan">Buat Laporan Sekarang</a>
            </div>

            </div>
        </section>
        <!-- ======= Contact Section ======= -->
        <section id="pengaduan" class="contact section-bg" style="padding: 120px 0 60px 0">
            <div class="container" data-aos="fade-up">
    
            <div class="section-title">
                <h2>Pengaduan Online</h2>
                <p style="text-transform: initial;">Rs. Bhayangkara</p>
            </div>
                <div class="row">
                    <div class="col-lg-6">
                        <form action="{{ route('login') }}" method="post" role="form" class="php-email-form">
                            @csrf
                            <div class="complaint-form-box">
                                <div class="select-complaint">Login</div>
                            </div>
                            <div class="form-group">
                                <input type="email" class="form-control" name="email" id="email" placeholder="mail@rstrijata.com" required/>
                                <div class="validate"></div>
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control" name="password" id="password" placeholder="............" required/>
                                <div class="validate"></div>
                            </div>
                            <div class="text-center"><button type="submit">Login</button></div>
                        </form>
                    </div>
                    <div class="col-lg-6">
                    <div class="row">
                        <div class="col-md-12">
                        <div class="info-box">
                            <i class="bx bx-map"></i>
                            <h3>Rs. Bhayangkara Denpasar</h3>
                            <p>Jl. Trijata No. 32 Sumerta Kelod 2F, Denpasar - BALI.</p>
                        </div>
                        </div>
                    </div>
    
                </div>
            </div>
        </section><!-- End Contact Section -->
      
@endsection
@section('js')
    <script>
        function Anonim() {
            // Get the checkbox
            var checkBox = document.getElementById("anonim");
            // Get the output text
            var identitas = document.getElementById("identitas");
        
            // If the checkbox is checked, display the output text
            if (checkBox.checked == true){
                identitas.style.display = "none";
            } else {
                identitas.style.display = "block";
            }
        }
    </script>
    <script>
		// Modal
		$(document).ready(function() {
			$.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
			// Tambah/Ubah Data
			$('#form').on('submit', function(e) {
				e.preventDefault();

				var formData = new FormData(this);

				$.ajax({
					url: $(this).attr('action'),
					type: "POST",
					data: formData,
					contentType: false,
					processData: false
				}).done(function(response){
					$('#modal').modal('hide');
					formExecuted(response);
				});
			});
		});

        function formExecuted(response) {
			var status = '';
			if (response.id == 1) {
				status = 'success';
				location.href ='{{url('/home')}}';
			} else {
				status = 'error';
			}
			toastr[status](response.keterangan, response.status, {
				closeButton: true,
				tapToDismiss: true
			});
		}
	</script>
@endsection
