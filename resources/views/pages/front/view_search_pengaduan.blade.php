@extends('front.layouts.app')
@section('title', 'Cek Pengaduan')
@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection
@section('content')
        <section id="why-us" class="why-us section-bg" style="padding-top:90px !important;">
            <div class="container-fluid" data-aos="fade-up">
      
              <div class="row">
      
                <div class="col-lg-5 align-items-stretch video-box" style='background-image: url("{{ asset('images/rs_trijata_img.jpg') }}");' data-aos="zoom-in" data-aos-delay="100">
                </div>
      
                <div class="col-lg-7 d-flex flex-column justify-content-center align-items-stretch">
                @if ($data->status==0)
                    <div class="content">
                        <h3>Status Laporan Pengaduan dengan No. HP  <strong>{{ $ticket }}</strong></h3>
                        <p>
                            No HP yang Anda masukkan tidak ditemukan dalam Sistem Kami! Silahkan periksa kembali No. HP Laporan Anda.
                        </p>
                    </div>
                @else
                    @php
                        $status = "";
                        if($data->peng_verifikasi=="0"){
                            $status = "Pengaduan Anda menunggu Verifikasi oleh Admin Sistem.";
                        }elseif ($data->peng_verifikasi=="1") {
                            $status = "Pengaduan Anda telah Verifikasi oleh Admin Sistem dan menunggu tindak lanjut.";
                        }elseif ($data->peng_verifikasi=="2") {
                            $status = "Pengaduan Anda dianggap menyimpang oleh Admin Sistem.";
                        }elseif ($data->peng_verifikasi=="3") {
                            $status = "Pengaduan Anda telah Verifikasi dan Ditindak Lanjuti.";
                        }
                    @endphp
                    <div class="content">
                        <h3>Status Laporan Pengaduan dengan ID Tikcet<strong>{{$ticket}}</strong></h3>
                        <p>
                            Terima kasih telah membuat Laporan Pengaduan kepada Kami, guna meningkatkan pelayanan kepada Masyarakat Umum.
                        </p>
                    </div>
        
                    <div class="accordion-list">
                        <ul>
                        <li>
                            <a  class="collapse" href="#"><span>1</span>  {{$status}} </a>
                            <div id="accordion-list-1" class="collapse show" data-parent=".accordion-list">
                            </div>
                        </li>
                        @php
                            $i = 2;
                        @endphp
                        @foreach ($tindaklanjut as $tl)
                            <li>
                                <a  class="collapse" href="#"><span>{{$i}}</span>  Tindak Lanjut dari {{$tl->name}} <small class="mail-date-time text-muted pull-right">{{date("F j, Y, g:i a",strtotime($tl->created_at))}}</small></a>
                                <p>{{$tl->pengt_tindaklanjut}}</p>
                                @if ($tl->pengt_file!="")
                                                <div class="card-footer">
                                                    <div class="mail-attachments">
                                                        <div class="d-flex align-items-center mb-1">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-paperclip font-medium-1 mr-50"><path d="M21.44 11.05l-9.19 9.19a6 6 0 0 1-8.49-8.49l9.19-9.19a4 4 0 0 1 5.66 5.66l-9.2 9.19a2 2 0 0 1-2.83-2.83l8.49-8.48"></path></svg>
                                                            <h6 class="font-weight-bolder text-body mb-0">File Tindak Lanjut</h6>
                                                        </div>
                                                        <div class="d-flex flex-column">
                                                            <a href="{{url('files')}}/{{$tl->pengt_file}}" target="_blank" class="mb-50">
                                                                <img src="{{asset('app-assets/images/icons/doc.png')}}" class="mr-25" alt="png" height="18">
                                                                <small class="text-muted font-weight-bolder">{{$tl->pengt_file}}</small>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                <div id="accordion-list-1" class="collapse show" data-parent=".accordion-list">
                                </div>
                            </li>
                            @php
                                $i++
                            @endphp
                        @endforeach
                        @if ($data->peng_verifikasi=="3")
                            <li>
                                <a  class="collapse" href="#"><span>{{$i}}</span>  Terima kasih! Laporan Anda telah ditutup! </a>
                                <div id="accordion-list-1" class="collapse show" data-parent=".accordion-list">
                                </div>
                            </li>
                        @endif
                        </ul>
                  </div>
                @endif
                  
      
                </div>
      
              </div>
      
            </div>
          </section><!-- End Why Us Section -->
@endsection
@section('js')
    <script>
		// Modal
		$(document).ready(function() {
			$.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
			// Tambah/Ubah Data
			$('#form').on('submit', function(e) {
				e.preventDefault();

				var formData = new FormData(this);

				$.ajax({
					url: $(this).attr('action'),
					type: "POST",
					data: formData,
					contentType: false,
					processData: false
				}).done(function(response){
					$('#modal').modal('hide');
					formExecuted(response);
				});
			});
		});

        function formExecuted(response) {
			var status = '';
			if (response.id == 1) {
				status = 'success';
				location.href ='{{url('/home')}}';
			} else {
				status = 'error';
			}
			toastr[status](response.keterangan, response.status, {
				closeButton: true,
				tapToDismiss: true
			});
		}
	</script>
@endsection
