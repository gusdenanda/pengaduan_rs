<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>@yield('title') | RS Bhayangkara Dps</title>
  <meta content="Pengaduan Online Rumah Sakit Bhayangkara Denpasar" name="description">
  <meta content="Pengaduan Online Rumah Sakit Bhayangkara Denpasar" name="keywords">

  <!-- Favicons -->
  <link href="{{ asset('images/logo-rs.png') }}" rel="icon">
  <link href="{{ asset('images/logo-rs.png') }}" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Vendor CSS Files -->
  <link href="{{asset('assets/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
  <link href="{{asset('assets/vendor/icofont/icofont.min.css')}}" rel="stylesheet">
  <link href="{{asset('assets/vendor/boxicons/css/boxicons.min.css"')}} rel="stylesheet">
  <link href="{{asset('assets/vendor/animate.css/animate.min.css')}}" rel="stylesheet">
  <link href="{{asset('assets/vendor/remixicon/remixicon.css')}}" rel="stylesheet">
  <link href="{{asset('assets/vendor/venobox/venobox.css')}}" rel="stylesheet">
  <link href="{{asset('assets/vendor/owl.carousel/assets/owl.carousel.min.css')}}" rel="stylesheet">
  <link href="{{asset('assets/vendor/aos/aos.css')}}" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="{{asset('assets/css/style.css')}}" rel="stylesheet">
  @yield('css')

  <!-- =======================================================
  * Template Name: Multi - v2.2.1
  * Template URL: https://bootstrapmade.com/multi-responsive-bootstrap-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>

  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top">
    <div class="container d-flex align-items-center">

      <h1 class="logo mr-auto"><a href="index.php">SIPOLAR</a></h1>
      <!-- Uncomment below if you prefer to use an image logo -->
      <!-- <a href="index.html" class="logo mr-auto"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->

      @include('front.layouts.header')

    </div>
  </header><!-- End Header -->
   <main id="main">
        @yield('content')
  </main><!-- End #main -->
  @include('front.layouts.footer')
  <div id="preloader"></div>
  <a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>

  <!-- Vendor JS Files -->
  <script src="{{ asset ('assets/vendor/jquery/jquery.min.js')}}"></script>
  <script src="{{ asset ('assets/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
  <script src="{{ asset ('assets/vendor/jquery.easing/jquery.easing.min.js')}}"></script>
  <script src="{{ asset ('assets/vendor/waypoints/jquery.waypoints.min.js')}}"></script>
  <script src="{{ asset ('assets/vendor/counterup/counterup.min.js')}}"></script>
  <script src="{{ asset ('assets/vendor/venobox/venobox.min.js')}}"></script>
  <script src="{{ asset ('assets/vendor/owl.carousel/owl.carousel.min.js')}}"></script>
  <script src="{{ asset ('assets/vendor/isotope-layout/isotope.pkgd.min.js')}}"></script>
  <script src="{{ asset ('assets/vendor/aos/aos.js')}}"></script>

  <!-- Template Main JS File -->
  <script src="{{ asset ('assets/js/main.js')}}"></script>
  @yield('js')
  <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        });
    </script>

</body>

</html>