<footer id="footer">
    <div class="footer-top">
      <div class="container">
        <div class="row">

          <div class="col-lg-4 col-md-6">
            <div class="footer-info">
              <h3>Rs. Bhayangkara Denpasar</h3>
              <p class="pb-3"><em>Jl. Trijata No. 32 Sumerta Kelod 2F, Denpasar - BALI.</em></p>
              <p>
                <strong>Phone:</strong> 0361-234670<br>
                <strong>Email:</strong> rstrijata@gmail.com<br>
              </p>
              <div class="social-links mt-3">
                <a href="https://web.facebook.com/rstrijatabhayangkaradenpasar/" target="_blank" class="facebook"><i class="fa fa-facebook"></i></a>
                <a href="https://www.instagram.com/rsbhayangkaradenpasar/" target="_blank" class="instagram"><i class="fa fa-instagram"></i></a>
                
              </div>
            </div>
          </div>

          <div class="col-lg-2 col-md-6 footer-links">
            <h4>Link</h4>
            <ul>
              <li><i class="fa fa-chevron-right"></i> <a href="/home">Home</a></li>
              <li><i class="fa fa-chevron-right"></i> <a href="#pengaduan">Buat Laporan Sekarang</a></li>
              <li><i class="fa fa-chevron-right"></i> <a href="/login">Login</a></li>
              <li><i class="fa fa-chevron-right"></i> <a href="/register">Register</a></li>
            </ul>
          </div>

          <div class="col-lg-2 col-md-6 footer-links">
            <h4>Tautan Pintas</h4>
            <ul>
              <li><i class="fa fa-chevron-right"></i> <a href="https://rstrijata.com/id/Beranda" target="_blank">Rs Bhayangkara Denpasar</a></li>
              <li><i class="fa fa-chevron-right"></i> <a href="https://eswab.rstrijata.com" target="_blank">E-Swab</a></li>
            </ul>
          </div>

          <div class="col-lg-4 col-md-6 footer-newsletter">
            <h4>Pengaduan Online</h4>
            <p>Pengaduan Online rumah Sakit Bhayangkara merupakan sebuah platform yang diperuntukkan kepada Masyarakat Umum / Tenaga Medis / Pegawai Rumah Sakit Bhayangkara Denpasar untuk menyampaikan Aspirasi dan Pengaduan.</p>
          </div>

        </div>
      </div>
    </div>

    <div class="container">
      <div class="copyright">
        &copy; Copyright <strong><span>Rs. Bhayangkara Denpasar</span></strong>. All Rights Reserved <br>
        <a href="http://arcom.id/" target="_blank">PT. Teknologi Arcom Mediaksa (AM Tech)</a>
      </div>
      <div class="credits">
        <!-- All the links in the footer should remain intact. -->
        <!-- You can delete the links only if you purchased the pro version. -->
        <!-- Licensing information: https://bootstrapmade.com/license/ -->
        <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/multi-responsive-bootstrap-template/ -->
        Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
      </div>
    </div>
  </footer><!-- End Footer -->