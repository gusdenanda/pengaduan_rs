<?php

namespace App\Http\Controllers\Pengawas;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Pengaduan;
use App\Models\PengaduanTindakLanjut;
use App\Models\Departemen;
use App\Models\StatusTindaklanjut;
use App\Models\KategoriPengaduan;
use DB;
use Carbon\Carbon;

class DashboardController extends Controller
{
    public function index()
    {
        $total_pengaduan = Pengaduan::count(); 
        $total_pengaduan_verifikasi = Pengaduan::where('peng_verifikasi','1')->count(); 
        $total_pengaduan_menyimpang = Pengaduan::where('peng_verifikasi','2')->count(); 
        $total_pengaduan_tl = Pengaduan::where('peng_tindak_lanjut','=','1')->count(); 
        $total_pengaduan_btl = Pengaduan::where('peng_tindak_lanjut','=','0')->count(); 
        $persentase_positif = round(($total_pengaduan_tl/$total_pengaduan),2) * 100;
        $persentase_negatif = round(($total_pengaduan_btl/$total_pengaduan),2) * 100;

        $total_kategori_departemen = Pengaduan::select(DB::raw('departemen.dept_nama, COUNT(*) AS count,  pengaduan.peng_tindak_lanjut'))
                    ->join('departemen', 'departemen.id', '=', 'pengaduan.dept_id')
                    ->groupBy('departemen.dept_nama')
                    ->groupBy('pengaduan.peng_tindak_lanjut')
                    ->orderBy('departemen.dept_nama', 'ASC')->get();

        $users = Pengaduan::select('id', 'created_at')
            ->get()
            ->groupBy(function ($date) {
                return Carbon::parse($date->created_at)->format('m');
            });

        $usermcount = [];
        $blnpos = [];

        foreach ($users as $key => $value) {
            $usermcount[(int)$key] = count($value);
        }

        $month = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

        for ($i = 1; $i <= 12; $i++) {
            if (!empty($usermcount[$i])) {
                $blnpos[$i]['count'] = $usermcount[$i];
            } else {
                $blnpos[$i]['count'] = 0;
            }
            $blnpos[$i]['month'] = $month[$i - 1];
        }
        
        $array_icon = array("bg-light-primary","bg-light-warning","bg-light-success","bg-light-primary","bg-light-warning","bg-light-success","bg-light-primary","bg-light-warning");
        return view('pages.pengawas.dashboard',compact('total_pengaduan','array_icon','total_pengaduan_verifikasi','total_pengaduan_menyimpang','total_pengaduan_tl','total_kategori_departemen','blnpos','persentase_positif','persentase_negatif'));
    }
}
