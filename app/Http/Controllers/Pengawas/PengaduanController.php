<?php

namespace App\Http\Controllers\Pengawas;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Pengaduan;
use App\Models\PengaduanTindakLanjut;
use App\Models\Departemen;
use App\Models\StatusTindaklanjut;
use App\Models\KategoriPengaduan;

use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Validator;
use Auth;

class PengaduanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dept = Departemen::select('id', 'dept_nama')
                            ->where('dept_active', '1')
                            ->get();
        $kp = KategoriPengaduan::select('id', 'kp_nama')
                            ->where('kp_active', '1')
                            ->get();
        return view('pages.pengawas.pengaduan',compact('dept','kp'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function status($id, $keterangan)
    {
        if ($id == 1) {
            $status = 'Berhasil';
        } else {
            $status = 'Gagal';
        }

        return [
            'id'            => $id,
            'status'        => $status,
            'keterangan'    => $keterangan
        ];
    }
    public function data()
    {
        $user = Auth::user();
        $dept_id = $user->dept_id;
        $data = Pengaduan::join('status_tindaklanjut', 'pengaduan.stl_id', 'status_tindaklanjut.id')
                            ->select(['pengaduan.id', 'pengaduan.peng_tgl','pengaduan.peng_nama','pengaduan.peng_topik', 'pengaduan.peng_verifikasi','status_tindaklanjut.stl_nama'])
                            ->whereIn('peng_verifikasi', array(1, 3)) 
                            ->orderBy('pengaduan.peng_tgl', 'DESC');
        
        return DataTables::of($data)
            ->addColumn('aksi', function ($item) {
                return '<form action="penandatangan/destroy/'. $item->id .'" class="text-center" method="POST">
                            <div class="btn-group">
                                <a href="' . route('pengawas.pengaduan.viewdetail', $item->id) .'"><button type="button" class="btn btn-info">
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="1.3em" height="1.3em" style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" preserveAspectRatio="xMidYMid meet" viewBox="0 0 24 24"><g fill="none" stroke="#fff" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><circle cx="12" cy="12" r="10"/><path d="M12 16v-4"/><path d="M12 8h.01"/></g></svg>
                                </button>
                                </a>
                            </div>
                        </form>';
            })
            ->addColumn('tl', function ($item) {
                $tl = PengaduanTindakLanjut::count();
                if($tl>0){
                    $st = '<div disabled class="badge badge-md badge-success">Sudah</div>';
                }else{
                    $st = '<div disabled class="badge badge-md badge-danger">Belum</div>';
                }
                return $st;
            })
            ->addColumn('peng_verifikasi', function ($item) {
                if($item->peng_verifikasi=="1"){
                    $st = '<div disabled class="badge badge-md badge-success">Sudah Diverifikasi</div>';
                }elseif($item->peng_verifikasi=="2"){
                    $st = '<div disabled class="badge badge-md badge-danger">Penyimpangan</div>';
                }elseif($item->peng_verifikasi=="3"){
                    $st = '<div disabled class="badge badge-md badge-primary">Laporan Ditutup</div>';
                }else{
                    $st = '<div disabled class="badge badge-md badge-warning">Belum Diverifikasi</div>';
                }
                return $st;
            })
            ->rawColumns(['aksi','peng_verifikasi','tl'])
            ->removeColumn('id')
            ->make(true);
    }

    public function viewDetail($id)
    {
        $dept = Departemen::select('id', 'dept_nama')
                            ->where('dept_active', '1')
                            ->get();
        $kp = KategoriPengaduan::select('id', 'kp_nama')
                            ->where('kp_active', '1')
                            ->get();
        $stl = StatusTindaklanjut::select('id', 'stl_nama')
                            ->where('stl_active', '1')
                            ->get();
        $pengaduan = Pengaduan::where('id', $id)->first();

        $tindaklanjut = PengaduanTindakLanjut::select(
                            'pengaduan_tindaklanjut.id', 
                            'pengaduan_tindaklanjut.peng_id', 
                            'pengaduan_tindaklanjut.user_id',
                            'pengaduan_tindaklanjut.pengt_tindaklanjut', 
                            'pengaduan_tindaklanjut.pengt_file', 
                            'pengaduan_tindaklanjut.created_at',
                            'users.name',
                            'users.email',
                            'users.photo',
                        )
                        ->join("users", "users.id", "=", "pengaduan_tindaklanjut.user_id")
                        ->where('peng_id', $id)
                        ->orderBy('created_at', 'DESC')
                        ->get(); 
        return view('pages.pengawas.pengaduandetail',compact('pengaduan','dept','kp','tindaklanjut','stl'));
    }

    public function storetindaklanjut(Request $request)
    {
        $pf = "";
        if ($request->pengt_file != NULL) {
            $validator = Validator::make($request->all(), [
                'pengt_file'      => 'nullable|max:1024'
            ]);
            if ($validator->fails()) {
                return $this->status(0, $validator->errors()->first());
            }
            $file = $request->file('pengt_file');
            $pf = rand() . '.' . $file->getClientOriginalExtension();
            $file->move('files', $pf);
        }

        $user = Auth::user();
        $id_user = $user->id;
        PengaduanTindakLanjut::create([
            'user_id' => $id_user,
            'pengt_tindaklanjut' => $request->pengt_tindaklanjut,
            'stl_id' => $request->stl_id,
            'peng_id' => $request->peng_id,
            'pengt_file' => $pf
        ]);
        Pengaduan::where('id', $request->peng_id)->update([
            'stl_id' => $request->stl_id,
            'peng_tindak_lanjut' => 1
        ]);
    
        return $this->status(1, 'Tindak Lanjut berhasil ditambah');
    }
    public function edittindaklanjut($id)
    {
        $data = PengaduanTindakLanjut::where('id', $id)->first();
        return json_encode($data);
    }

    public function updatetindaklanjut(Request $request, $id)
    {
        $pf = "";
        if ($request->pengt_file != NULL) {
            $validator = Validator::make($request->all(), [
                'pengt_file'      => 'nullable|max:1024'
            ]);
            if ($validator->fails()) {
                return $this->status(0, $validator->errors()->first());
            }
            $file = $request->file('pengt_file');
            $pf = rand() . '.' . $file->getClientOriginalExtension();
            $file->move('files', $pf);
            PengaduanTindakLanjut::where('id', $id)->update([
                'pengt_tindaklanjut' => $request->pengt_tindaklanjut,
                'pengt_file' => $pf,
                'stl_id' => $request->stl_id
            ]);
            Pengaduan::where('id', $request->peng_id)->update([
                'stl_id' => $request->stl_id
            ]);
        }else{
            PengaduanTindakLanjut::where('id', $id)->update([
                'pengt_tindaklanjut' => $request->pengt_tindaklanjut,
                'stl_id' => $request->stl_id
            ]);
            Pengaduan::where('id', $request->peng_id)->update([
                'stl_id' => $request->stl_id
            ]);
        }
        return $this->status(1, 'Tindaklanjut berhasil diubah');
    }

    public function tutuppengaduan(Request $request, $id)
    {
        $pf = "";
        Pengaduan::where('id', $id)->update([
                'peng_verifikasi' => 3
            ]);
        return $this->status(1, 'Pengaduan berhasil ditutup');
    }

    public function bukapengaduan(Request $request, $id)
    {
        $pf = "";
        Pengaduan::where('id', $id)->update([
                'peng_verifikasi' => 1
            ]);
        return $this->status(1, 'Pengaduan berhasil dibuka');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroytindaklanjut($id)
    {
        $pengaduan = PengaduanTindakLanjut::where('id', $id)->first(); 
        $total = PengaduanTindakLanjut::where('peng_id','=',$pengaduan->peng_id)->count(); 
        PengaduanTindakLanjut::where('id', $id)->delete();
        if($total==1){
            Pengaduan::where('id', $id)->update([
                'peng_tindak_lanjut' => 0
            ]);
        }
        return $this->status(1, 'Tindaklanjut berhasil dihapus');
    }
}
