<?php

namespace App\Http\Controllers\Admin\Master;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\ListRole;
use App\Models\RoleUser;
use App\Models\User;
use App\Models\Pengaduan;
use App\Models\Departemen;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Auth;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dept = Departemen::select('id', 'dept_nama')
                            ->where('dept_active', '1')
                            ->get();
        $data = User::getUser();
        $total_pengaduan_bv = Pengaduan::where('peng_verifikasi','0')->count(); 
        return view('pages.admin.user', compact('data','dept','total_pengaduan_bv'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email'     => 'unique:App\Models\User,email',
            'password' => 'required|string|confirmed|min:8',
            'name'      => 'required',
        ]);
        
        if ($validator->fails()) {
            return $this->status(0, $validator->errors()->first());
        }
        
        $user = User::storeUser($request);
        ListRole::storeListRole($user->id, $request->role);
        
        return $this->status(1, 'Data berhasil ditambah');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return json_encode(User::firstUserDataUser($user->id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $validator = Validator::make($request->all(), [
            'email'     => Rule::unique('users', 'email')->ignore($user->id),
            'name'      => 'required',
        ]);
        if ($validator->fails()) {
            return $this->status(0, $validator->errors()->first());
        }
        
        User::updateUser($user->id, $request);
        ListRole::storeListRole($user->id, $request->role);
        
        return $this->status(1, 'Data berhasil diganti');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        if (Auth::id() == $user->id) {
            return $this->status(0, 'User tidak dapat dihapus');
        }

        User::deleteUser($user->id);
        return $this->status(1, 'User berhasil dihapus');
    }
    public function status($id, $keterangan)
    {
        if ($id == 1) {
            $status = 'Berhasil';
        } else {
            $status = 'Gagal';
        }

        return [
            'id'            => $id,
            'status'        => $status,
            'keterangan'    => $keterangan
        ];
    }

    public function data()
    {
        $user = User::select(['id', 'email', 'name','no_hp']);
        
        return DataTables::of($user)
            ->addColumn('role', function ($item) {
                $role = ListRole::selectRaw('(CASE role_id WHEN 1 THEN "Admin" WHEN 2 THEN "User Pelapor" WHEN 3 THEN "Departemen" ELSE "Pengawas" END) as role')->where('user_id', $item->id)->get();
                
                $roles = "";
                foreach ($role as $key => $value) {
                    $roles .= '<li class="list-group-item">'.$value->role.'</li>';
                }
                return '<ul class="list-group">'.$roles.'</ul>';
            })
            ->addColumn('aksi', function ($item) {
                return '<form action="/admin/master/kewarganegaraan/destroy/'. $item->id .'" class="text-center" method="POST"><div class="btn-group"><button type="button" class="btn btn-warning ubah" data-value="'. $item->id .'"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit-2"><path d="M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z"></path></svg></button><button type="button" class="btn btn-danger hapus" onclick="hapus('. $item->id .')"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path></svg></button></div></form>';
            })
            ->rawColumns(['role', 'aksi'])
            ->removeColumn('id')
            ->make(true);
    }
}
