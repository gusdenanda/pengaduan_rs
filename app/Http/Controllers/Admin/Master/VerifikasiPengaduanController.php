<?php

namespace App\Http\Controllers\Admin\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Pengaduan;
use App\Models\Departemen;
use App\Models\KategoriPengaduan;

use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Validator;
use Auth;

class VerifikasiPengaduanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dept = Departemen::select('id', 'dept_nama')
                            ->where('dept_active', '1')
                            ->get();
        $kp = KategoriPengaduan::select('id', 'kp_nama')
                            ->where('kp_active', '1')
                            ->get();
        $total_pengaduan_bv = Pengaduan::where('peng_verifikasi','0')->count(); 
        return view('pages.admin.verifikasi_pengaduan',compact('dept','kp','total_pengaduan_bv'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'peng_file'      => 'nullable|max:1024'
        ]);
        if ($validator->fails()) {
            return $this->status(0, $validator->errors()->first());
        }
        $pf = "";
        if ($request->peng_file != NULL) {
            $file = $request->file('peng_file');
            $pf = rand() . '.' . $file->getClientOriginalExtension();
            $file->move('files', $pf);
        }

        /*$id_user = auth()->user()->id;
        if($id_user!=""){
            Pengaduan::create([
                'peng_tgl' => $request->peng_tgl,
                'user_id' => $request->user_id,
                'peng_nama' => $request->peng_nama,
                'peng_hp' => $request->peng_hp,
                'peng_email' => $request->peng_email,
                'kp_id' => $request->kp_id,
                'peng_topik' => $request->peng_topik,
                'peng_isi_pengaduan' => $request->peng_isi_pengaduan,
                'peng_file' => $pf,
                'peng_identitas_pelapor' => $request->peng_identitas_pelapor,
                'peng_sifat_pengaduan' => $request->peng_sifat_pengaduan,
                'dept_id'  => $request->dept_id,
                'stl_id'  => $request->stl_id,
                'peng_verifikasi'  => $request->peng_verifikasi

            ]);
        }else{*/
            Pengaduan::create([
                'peng_tgl' => $request->peng_tgl,
                'peng_nama' => $request->peng_nama,
                'peng_hp' => $request->peng_hp,
                'peng_email' => $request->peng_email,
                'kp_id' => $request->kp_id,
                'peng_topik' => $request->peng_topik,
                'peng_isi_pengaduan' => $request->peng_isi_pengaduan,
                'peng_file' => $pf,
                'peng_identitas_pelapor' => $request->peng_identitas_pelapor,
                'peng_sifat_pengaduan' => $request->peng_sifat_pengaduan,
                'dept_id'  => $request->dept_id,
                'stl_id'  => $request->stl_id,
                'peng_verifikasi'  => $request->peng_verifikasi
            ]);
        //}
        return $this->status(1, 'Pengaduan berhasil ditambah');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Pengaduan::where('id', $id)->first();
        return json_encode($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $pf = "";
        if ($request->peng_file != NULL) {
            $validator = Validator::make($request->all(), [
                'peng_file'      => 'nullable|max:1024'
            ]);
            if ($validator->fails()) {
                return $this->status(0, $validator->errors()->first());
            }
            $file = $request->file('peng_file');
            $pf = rand() . '.' . $file->getClientOriginalExtension();
            $file->move('files', $pf);
            
            /*$id_user = auth()->user()->id;
            if($id_user!=""){
                Pengaduan::where('id', $id)->update([
                    'peng_tgl' => $request->peng_tgl,
                    'user_id' => $request->user_id,
                    'peng_nama' => $request->peng_nama,
                    'peng_hp' => $request->peng_hp,
                    'peng_email' => $request->peng_email,
                    'kp_id' => $request->kp_id,
                    'peng_topik' => $request->peng_topik,
                    'peng_isi_pengaduan' => $request->peng_isi_pengaduan,
                    'peng_file' => $pf,
                    'peng_identitas_pelapor' => $request->peng_identitas_pelapor,
                    'peng_sifat_pengaduan' => $request->peng_sifat_pengaduan,
                    'dept_id'  => $request->dept_id,
                    'peng_verifikasi'  => "1",
                    'stl_id' => "2"
                ]);
            }else{*/
                Pengaduan::where('id', $id)->update([
                    'peng_tgl' => $request->peng_tgl,
                    'peng_nama' => $request->peng_nama,
                    'peng_hp' => $request->peng_hp,
                    'peng_email' => $request->peng_email,
                    'kp_id' => $request->kp_id,
                    'peng_topik' => $request->peng_topik,
                    'peng_isi_pengaduan' => $request->peng_isi_pengaduan,
                    'peng_file' => $pf,
                    'peng_identitas_pelapor' => $request->peng_identitas_pelapor,
                    'peng_sifat_pengaduan' => $request->peng_sifat_pengaduan,
                    'dept_id'  => $request->dept_id,
                    'peng_verifikasi'  => "1",
                    'stl_id' => "2"
                ]);
            //}

        }else{
            /*$id_user = auth()->user()->id;
            if($id_user!=""){
                Pengaduan::where('id', $id)->update([
                    'peng_tgl' => $request->peng_tgl,
                    'user_id' => $request->user_id,
                    'peng_nama' => $request->peng_nama,
                    'peng_hp' => $request->peng_hp,
                    'peng_email' => $request->peng_email,
                    'kp_id' => $request->kp_id,
                    'peng_topik' => $request->peng_topik,
                    'peng_isi_pengaduan' => $request->peng_isi_pengaduan,
                    'peng_identitas_pelapor' => $request->peng_identitas_pelapor,
                    'peng_sifat_pengaduan' => $request->peng_sifat_pengaduan,
                    'dept_id'  => $request->dept_id,
                    'peng_verifikasi'  => "1",
                    'stl_id' => "2"
                ]);
            }else{*/
                Pengaduan::where('id', $id)->update([
                    'peng_tgl' => $request->peng_tgl,
                    'peng_nama' => $request->peng_nama,
                    'peng_hp' => $request->peng_hp,
                    'peng_email' => $request->peng_email,
                    'kp_id' => $request->kp_id,
                    'peng_topik' => $request->peng_topik,
                    'peng_isi_pengaduan' => $request->peng_isi_pengaduan,
                    'peng_identitas_pelapor' => $request->peng_identitas_pelapor,
                    'peng_sifat_pengaduan' => $request->peng_sifat_pengaduan,
                    'dept_id'  => $request->dept_id,
                    'peng_verifikasi'  => "1",
                    'stl_id' => "2"
                ]);
            //}
        }
        return $this->status(1, 'Pengaduan berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Pengaduan::where('id', $id)->first();
        if($data->peng_verifikasi=="2"){
            Pengaduan::where('id', $id)->update([
                'peng_verifikasi'  => "0"
            ]);
        }else{
            Pengaduan::where('id', $id)->update([
                'peng_verifikasi'  => "2"
            ]);
        }
        
        return $this->status(1, 'Pengaduan berhasil diupdate');
    }

    public function status($id, $keterangan)
    {
        if ($id == 1) {
            $status = 'Berhasil';
        } else {
            $status = 'Gagal';
        }

        return [
            'id'            => $id,
            'status'        => $status,
            'keterangan'    => $keterangan
        ];
    }
    public function data()
    {
        $data = Pengaduan::join('status_tindaklanjut', 'pengaduan.stl_id', 'status_tindaklanjut.id')
                            ->select(['pengaduan.id', 'pengaduan.peng_tgl','pengaduan.peng_nama','pengaduan.peng_topik', 'pengaduan.peng_verifikasi','status_tindaklanjut.stl_nama'])
                            ->where('peng_verifikasi','<>','1')
                            ->orderBy('pengaduan.peng_tgl', 'DESC');
        
        return DataTables::of($data)
            ->addColumn('aksi', function ($item) {
                return '<form action="penandatangan/destroy/'. $item->id .'" class="text-center" method="POST">
                            <div class="btn-group">
                                <button type="button" class="btn btn-success ubah" data-value="'. $item->id .'" data-nama="'. $item->peng_topik .'">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-activity"><polyline points="9 11 12 14 23 3"></polyline><path d="M21 12v7a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h11"></path></svg>
                                </button>
                            </div>
                        </form>';
            })
            ->addColumn('peng_verifikasi', function ($item) {
                if($item->peng_verifikasi=="1"){
                    $st = '<div disabled class="badge badge-md badge-success">Sudah Diverifikasi</div>';
                }elseif($item->peng_verifikasi=="2"){
                    $st = '<div disabled class="badge badge-md badge-danger">Penyimpangan</div>';
                }else{
                    $st = '<div disabled class="badge badge-md badge-warning">Belum Diverifikasi</div>';
                }
                return $st;
            })
            ->rawColumns(['aksi','peng_verifikasi'])
            ->removeColumn('id')
            ->make(true);
    }
}
