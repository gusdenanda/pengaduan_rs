<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Pengaduan;
use App\Models\Departemen;
use App\Models\User;

use Illuminate\Support\Facades\Validator;
use DateTime;
class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dept = Departemen::select('id', 'dept_nama')
                            ->where('dept_active', '1')
                            ->get();
        $message = array();
        $message["status"]=0;

        //statistik
        $jumlah_pengaduan = Pengaduan::count(); 
        $jumlah_pengaduan = number_format($jumlah_pengaduan);
        $jumlah_tindaklanjut = Pengaduan::where('stl_id','3')->count(); 
        $jumlah_tindaklanjut = number_format($jumlah_tindaklanjut);
        $jumlah_menyimpang = Pengaduan::where('peng_verifikasi','2')->count(); 
        $jumlah_menyimpang = number_format($jumlah_menyimpang);
        $jumlah_user = User::count(); 
        $jumlah_user = number_format($jumlah_user);
        return view('pages.front.home',compact('dept','message','jumlah_pengaduan','jumlah_tindaklanjut','jumlah_menyimpang','jumlah_user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'peng_file'      => 'nullable|max:1024'
        ]);
        if ($validator->fails()) {
            return $this->status(0, $validator->errors()->first());
        }
        $pf = "";
        if ($request->peng_file != NULL) {
            $file = $request->file('peng_file');
            $pf = rand() . '.' . $file->getClientOriginalExtension();
            $file->move('files', $pf);
        }else{
            return $this->status(0, "File Pendukung wajib diisi");
        }
        if ($request->peng_nama == "") {
            return $this->status(0, "Nama Pelapor wajib diisi");
        }
        if ($request->peng_hp == "") {
            return $this->status(0, "No. HP Pelapor wajib diisi");
        }
        if($request->peng_identitas_pelapor=="0"){
            $request->peng_nama = "Anonim";
            $request->peng_hp = "08x xxx xxx xxx";
            $request->peng_email = "anonim@mail.com";
        }else{
            $request->peng_identitas_pelapor="1";
        }
        if($request->peng_sifat_pengaduan=="0"){
            $peng_sifat_pengaduan="0";
        }else{
            $peng_sifat_pengaduan="1";
        }
        //generate ticket code
        $peng = Pengaduan::orderBy('id', 'DESC')->first();
        $inc = (int) $peng->id + 1;

        $date = new DateTime('now');

        $ticket = $date->format('Ymd') . "-" . str_pad($inc, 4, "0", STR_PAD_LEFT);
        Pengaduan::create([
                'peng_tgl' => date("Y-m-d"),
                'peng_nama' => $request->peng_nama,
                'peng_hp' => $request->peng_hp,
                'peng_email' => $request->peng_email,
                'kp_id' => $request->kp_id,
                'peng_topik' => $request->peng_topik,
                'peng_isi_pengaduan' => $request->peng_isi_pengaduan,
                'peng_file' => $pf,
                'peng_identitas_pelapor' => $request->peng_identitas_pelapor,
                'peng_sifat_pengaduan' => $peng_sifat_pengaduan,
                'dept_id'  => $request->dept_id,
                'stl_id'  => "1",
                'peng_verifikasi'  => "0",
                'peng_ticket' => $ticket
            ]);
        $message = array();
        $message["status"]=1;
        $message["ticket"] = $ticket;
        $dept = Departemen::select('id', 'dept_nama')
                            ->where('dept_active', '1')
                            ->get();
        //statistik
        $jumlah_pengaduan = Pengaduan::count(); 
        $jumlah_pengaduan = number_format($jumlah_pengaduan);
        $jumlah_tindaklanjut = Pengaduan::where('stl_id','3')->count(); 
        $jumlah_tindaklanjut = number_format($jumlah_tindaklanjut);
        $jumlah_menyimpang = Pengaduan::where('peng_verifikasi','2')->count(); 
        $jumlah_menyimpang = number_format($jumlah_menyimpang);
        $jumlah_user = User::count(); 
        $jumlah_user = number_format($jumlah_user);
        return view('pages.front.home',compact('dept','message','jumlah_pengaduan','jumlah_tindaklanjut','jumlah_menyimpang','jumlah_user'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function status($id, $keterangan)
    {
        if ($id == 1) {
            $status = 'Berhasil';
        } else {
            $status = 'Gagal';
        }

        return [
            'id'            => $id,
            'status'        => $status,
            'keterangan'    => $keterangan
        ];
    }
}
