<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Pengaduan;
use App\Models\PengaduanTindakLanjut;

class CekPengaduanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.front.cek_pengaduan');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function searchPengaduan(Request $request)
    {
        $data = Pengaduan::where('peng_hp', $request->ticket)
                            ->orderBy('created_at', 'DESC')
                            ->first();
        if ( is_null($data) ) {
            $data = array();
            $data["status"]=0;
            $data["view"]="web";
            $data = (object) $data;
            $tindaklanjut = (object) $tindaklanjut;

        }else{
            $data["status"]=1;
            $data["view"]="web";
            $tindaklanjut = PengaduanTindakLanjut::select(
                'pengaduan_tindaklanjut.id', 
                'pengaduan_tindaklanjut.peng_id', 
                'pengaduan_tindaklanjut.user_id',
                'pengaduan_tindaklanjut.pengt_tindaklanjut', 
                'pengaduan_tindaklanjut.pengt_file', 
                'pengaduan_tindaklanjut.created_at',
                'users.name',
                'users.email',
                'users.photo',
            )
            ->join("users", "users.id", "=", "pengaduan_tindaklanjut.user_id")
            ->where('peng_id', $data->id)
            ->orderBy('created_at', 'DESC')
            ->get(); 
        }
        $ticket = $request->ticket;
        return view('pages.front.view_search_pengaduan',compact('data','ticket','tindaklanjut'));
    }
}
