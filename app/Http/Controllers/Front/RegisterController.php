<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ListRole;
use App\Models\RoleUser;
use App\Models\User;

use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Auth;

use App\Models\Pengaduan;
use Illuminate\Support\Facades\Hash;
class RegisterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('auth.register');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email'     => 'unique:App\Models\User,email',
            'password' => 'required|string|confirmed|min:8',
            'name'      => 'required',
        ]);
        
        if ($validator->fails()) {
            return $this->status(0, $validator->errors()->first());
        }
        

        if ($request->user_profile != NULL) {
            $validator = Validator::make($request->all(), [
                'user_profile'      => 'nullable|max:1024'
            ]);
            if ($validator->fails()) {
                return $this->status(0, $validator->errors()->first());
            }
            $file = $request->file('user_profile');
            $pf = rand() . '.' . $file->getClientOriginalExtension();
            $file->move('files', $pf);
            $user = User::create([
                'name'      => $request->name,
                'no_hp'     => $request->no_hp,
                'email'     => $request->email,
                'address'     => $request->address,
                'photo'       => $pf,
                'password'  => Hash::make($request->password)
            ]);
        }else{
            $user = User::create([
                'name'      => $request->name,
                'no_hp'     => $request->no_hp,
                'email'     => $request->email,
                'address'     => $request->address,
                'password'  => Hash::make($request->password)
            ]);
        }
        ListRole::storeListRole($user->id, $request->role);
        
        return view('auth.login');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function status($id, $keterangan)
    {
        if ($id == 1) {
            $status = 'Berhasil';
        } else {
            $status = 'Gagal';
        }

        return [
            'id'            => $id,
            'status'        => $status,
            'keterangan'    => $keterangan
        ];
    }
}
