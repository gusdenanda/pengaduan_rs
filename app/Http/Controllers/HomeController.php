<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Pengaduan;
use App\Models\Departemen;
use App\Models\User;
use Auth;

class HomeController extends Controller
{
    
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if (Auth::check()) {
			if (Auth::user()->hasRole('admin')) {
                return redirect('/admin/master/dashboard');
            } elseif (Auth::user()->hasRole('pelapor')) {
                return redirect('/pelapor/pengaduan');
            } elseif (Auth::user()->hasRole('departemen')) {
                return redirect('/departemen/dashboard');
            } elseif (Auth::user()->hasRole('pengawas')) {
                return redirect('/pengawas/dashboard');
            }
		}
        $dept = Departemen::select('id', 'dept_nama')
                ->where('dept_active', '1')
                ->get();
        $message = array();
        $message["status"]=0;

        //statistik
        $jumlah_pengaduan = Pengaduan::count(); 
        $jumlah_pengaduan = number_format($jumlah_pengaduan);
        $jumlah_tindaklanjut = Pengaduan::where('stl_id','3')->count(); 
        $jumlah_tindaklanjut = number_format($jumlah_tindaklanjut);
        $jumlah_menyimpang = Pengaduan::where('peng_verifikasi','2')->count(); 
        $jumlah_menyimpang = number_format($jumlah_menyimpang);
        $jumlah_user = User::count(); 
        $jumlah_user = number_format($jumlah_user);
        
        return view('pages.front.home',compact('dept','message','jumlah_pengaduan','jumlah_tindaklanjut','jumlah_menyimpang','jumlah_user'));
    }
}
