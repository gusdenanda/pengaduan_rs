<?php

namespace App\Http\Controllers\Pelapor;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Pengaduan;
use App\Models\Departemen;
use App\Models\KategoriPengaduan;
use Illuminate\Support\Facades\Validator;
use Auth;
class PengaduanDitolakController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $id_user = $user->id;

        $dept = Departemen::select('id', 'dept_nama')
                            ->where('dept_active', '1')
                            ->get();
        $kp = KategoriPengaduan::select('id', 'kp_nama')
                            ->where('kp_active', '1')
                            ->get();

        $pengaduan = Pengaduan::where('user_id', $id_user)
                    ->where('peng_verifikasi','2')
                    ->orderBy('created_at', 'DESC')->get();
        $count_pengaduan_stl = Pengaduan::where('user_id', $id_user)
                    ->where('stl_id','<>','1')
                    ->orderBy('created_at', 'DESC')->count();
        
        $count_pengaduan = Pengaduan::where('user_id', $id_user)->orderBy('created_at', 'DESC')->count();

        $count_pengaduan_ditolak = Pengaduan::where('user_id', $id_user)
                    ->where('peng_verifikasi','2')
                    ->orderBy('created_at', 'DESC')->count();
        
        return view('pages.pelapor.pengaduan_ditolak',compact('pengaduan','count_pengaduan','count_pengaduan_stl','count_pengaduan_ditolak','kp','dept'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
