<?php

namespace App\Http\Controllers\Pelapor;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Pengaduan;
use App\Models\PengaduanTindakLanjut;
use App\Models\Departemen;
use App\Models\StatusTindaklanjut;
use App\Models\KategoriPengaduan;

use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Validator;
use Auth;
class PengaduanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $id_user = $user->id;

        $dept = Departemen::select('id', 'dept_nama')
                            ->where('dept_active', '1')
                            ->get();
        $kp = KategoriPengaduan::select('id', 'kp_nama')
                            ->where('kp_active', '1')
                            ->get();
        
        return view('pages.pelapor.pengaduan_pelapor',compact('dept','kp'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'peng_file'      => 'nullable|max:1024'
        ]);
        if ($validator->fails()) {
            return $this->status(0, $validator->errors()->first());
        }
        $pf = "";
        if ($request->peng_file != NULL) {
            $file = $request->file('peng_file');
            $pf = rand() . '.' . $file->getClientOriginalExtension();
            $file->move('files', $pf);
        }

        $user = Auth::user();
        $id_user = $user->id;
        if($request->peng_identitas_pelapor=="0"){
            $request->peng_nama = "Anonim";
            $request->peng_hp = "08x xxx xxx xxx";
            $request->peng_email = "anonim@mail.com";
        }
         Pengaduan::create([
                'peng_tgl' => $request->peng_tgl,
                'user_id' => $id_user,
                'peng_nama' => $request->peng_nama,
                'peng_hp' => $request->peng_hp,
                'peng_email' => $request->peng_email,
                'kp_id' => $request->kp_id,
                'peng_topik' => $request->peng_topik,
                'peng_isi_pengaduan' => $request->peng_isi_pengaduan,
                'peng_file' => $pf,
                'peng_identitas_pelapor' => $request->peng_identitas_pelapor,
                'peng_sifat_pengaduan' => $request->peng_sifat_pengaduan,
                'dept_id'  => $request->dept_id,
                'stl_id'  => "1",
                'peng_verifikasi'  => "0"

            ]);
        
        return $this->status(1, 'Pengaduan berhasil ditambah');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Pengaduan::where('id', $id)->first();
        return json_encode($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $pf = "";
        if($request->peng_identitas_pelapor=="0"){
            $request->peng_nama = "Anonim";
            $request->peng_hp = "08x xxx xxx xxx";
            $request->peng_email = "anonim@mail.com";
        }
        if ($request->peng_file != NULL) {
            $validator = Validator::make($request->all(), [
                'peng_file'      => 'nullable|max:1024'
            ]);
            if ($validator->fails()) {
                return $this->status(0, $validator->errors()->first());
            }
            $file = $request->file('peng_file');
            $pf = rand() . '.' . $file->getClientOriginalExtension();
            $file->move('files', $pf);
            
            $id_user = auth()->user()->id;
            Pengaduan::where('id', $id)->update([
                    'peng_tgl' => $request->peng_tgl,
                    'peng_nama' => $request->peng_nama,
                    'peng_hp' => $request->peng_hp,
                    'peng_email' => $request->peng_email,
                    'kp_id' => $request->kp_id,
                    'peng_topik' => $request->peng_topik,
                    'peng_isi_pengaduan' => $request->peng_isi_pengaduan,
                    'peng_file' => $pf,
                    'peng_identitas_pelapor' => $request->peng_identitas_pelapor,
                    'peng_sifat_pengaduan' => $request->peng_sifat_pengaduan,
                    'dept_id'  => $request->dept_id
                ]);
        }else{
                Pengaduan::where('id', $id)->update([
                    'peng_tgl' => $request->peng_tgl,
                    'peng_nama' => $request->peng_nama,
                    'peng_hp' => $request->peng_hp,
                    'peng_email' => $request->peng_email,
                    'kp_id' => $request->kp_id,
                    'peng_topik' => $request->peng_topik,
                    'peng_isi_pengaduan' => $request->peng_isi_pengaduan,
                    'peng_identitas_pelapor' => $request->peng_identitas_pelapor,
                    'peng_sifat_pengaduan' => $request->peng_sifat_pengaduan,
                    'dept_id'  => $request->dept_id
                ]);
        }
        return $this->status(1, 'Pengaduan berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Pengaduan::where('id', $id)->delete();
        
        return $this->status(1, 'Pengaduan berhasil dihapus');
    }
    public function data()
    {
        $user = Auth::user();
        $id_user = $user->id;
        $data = Pengaduan::join('status_tindaklanjut', 'pengaduan.stl_id', 'status_tindaklanjut.id')
                            ->select(['pengaduan.id', 'pengaduan.peng_tgl','pengaduan.peng_nama','pengaduan.peng_topik', 'pengaduan.peng_verifikasi','status_tindaklanjut.stl_nama'])
                            ->where('pengaduan.user_id', $id_user);
        return DataTables::of($data)
            ->addColumn('aksi', function ($item) {
                if($item->peng_verifikasi=="1"){
                    return '<form action="penandatangan/destroy/'. $item->id .'" class="text-center" method="POST">
                                <div class="btn-group">
                                    <a href="' . route('pelapor.pengaduan.viewdetail', $item->id) .'">
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="1em" height="1em" style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" preserveAspectRatio="xMidYMid meet" viewBox="0 0 24 24"><g fill="none" stroke="#626262" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M1 12s4-8 11-8s11 8 11 8s-4 8-11 8s-11-8-11-8z"/><circle cx="12" cy="12" r="3"/></g></svg>
                                    </a>
                                </div>
                            </form>';
                }elseif($item->peng_verifikasi=="2"){
                     return '<form action="penandatangan/destroy/'. $item->id .'" class="text-center" method="POST">
                                <div class="btn-group">
                                    <a href="' . route('pelapor.pengaduan.viewdetail', $item->id) .'">
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="1em" height="1em" style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" preserveAspectRatio="xMidYMid meet" viewBox="0 0 24 24"><g fill="none" stroke="#626262" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M1 12s4-8 11-8s11 8 11 8s-4 8-11 8s-11-8-11-8z"/><circle cx="12" cy="12" r="3"/></g></svg>
                                    </a>
                                </div>
                            </form>';
                }elseif($item->peng_verifikasi=="3"){
                     return '<form action="penandatangan/destroy/'. $item->id .'" class="text-center" method="POST">
                                <div class="btn-group">
                                    <a href="' . route('pelapor.pengaduan.viewdetail', $item->id) .'">
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="1em" height="1em" style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" preserveAspectRatio="xMidYMid meet" viewBox="0 0 24 24"><g fill="none" stroke="#626262" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M1 12s4-8 11-8s11 8 11 8s-4 8-11 8s-11-8-11-8z"/><circle cx="12" cy="12" r="3"/></g></svg>
                                    </a>
                                </div>
                            </form>';
                }else{
                    return '<form action="penandatangan/destroy/'. $item->id .'" class="text-center" method="POST">
                            <div class="btn-group">
                                <a href="' . route('pelapor.pengaduan.viewdetail', $item->id) .'">
                                    <button type="button" class="btn btn-primary" data-value="'. $item->id .'" data-nama="'. $item->peng_topik .'"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false"  width="1em" height="1em" style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" preserveAspectRatio="xMidYMid meet" viewBox="0 0 24 24"><g fill="none" stroke="#fff" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M1 12s4-8 11-8s11 8 11 8s-4 8-11 8s-11-8-11-8z"/><circle cx="12" cy="12" r="3"/></g></svg></button>
                                </a>
                                <button type="button" class="btn btn-warning ubah" data-value="'. $item->id .'" data-nama="'. $item->peng_topik .'"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit-2"><path d="M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z"></path></svg></button>
                                <button type="button" class="btn btn-danger hapus" onclick="hapus('. $item->id .')"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path></svg></button>
                            </div>
                        </form>';
                }
               
            })
            ->addColumn('peng_verifikasi', function ($item) {
                if($item->peng_verifikasi=="1"){
                    $st = '<div disabled class="badge badge-md badge-success">Sudah Diverifikasi</div>';
                }elseif($item->peng_verifikasi=="2"){
                    $st = '<div disabled class="badge badge-md badge-danger">Penyimpangan</div>';
                }elseif($item->peng_verifikasi=="3"){
                    $st = '<div disabled class="badge badge-md badge-primary">Laporan Ditutup</div>';
                }else{
                    $st = '<div disabled class="badge badge-md badge-warning">Belum Diverifikasi</div>';
                }
                return $st;
            })
            ->rawColumns(['aksi','peng_verifikasi'])
            ->removeColumn('id')
            ->make(true);
    }
    public function status($id, $keterangan)
    {
        if ($id == 1) {
            $status = 'Berhasil';
        } else {
            $status = 'Gagal';
        }

        return [
            'id'            => $id,
            'status'        => $status,
            'keterangan'    => $keterangan
        ];
    }

    public function getUserLogin()
    {
        $user = Auth::user();
        return $user;
    }
    public function viewDetail($id)
    {
        $dept = Departemen::select('id', 'dept_nama')
                            ->where('dept_active', '1')
                            ->get();
        $kp = KategoriPengaduan::select('id', 'kp_nama')
                            ->where('kp_active', '1')
                            ->get();
        $stl = StatusTindaklanjut::select('id', 'stl_nama')
                            ->where('stl_active', '1')
                            ->get();
        $pengaduan = Pengaduan::where('id', $id)->first();

        $tindaklanjut = PengaduanTindakLanjut::select(
                            'pengaduan_tindaklanjut.id', 
                            'pengaduan_tindaklanjut.peng_id', 
                            'pengaduan_tindaklanjut.user_id',
                            'pengaduan_tindaklanjut.pengt_tindaklanjut', 
                            'pengaduan_tindaklanjut.pengt_file', 
                            'pengaduan_tindaklanjut.created_at',
                            'users.name',
                            'users.email',
                            'users.photo',
                        )
                        ->join("users", "users.id", "=", "pengaduan_tindaklanjut.user_id")
                        ->where('peng_id', $id)
                        ->orderBy('created_at', 'DESC')
                        ->get(); 
        return view('pages.pelapor.pengaduandetail',compact('pengaduan','dept','kp','tindaklanjut','stl'));
    }
}
