<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Departemen extends Model
{
    use HasFactory;
    protected $guarded = ['id','created_at','updated_at'];
    protected $table = 'departemen';

    static function storeDepartemen($request)
    {
        Departemen::create([
            'dept_nama' => $request->dept_nama,
            'dept_active'  => $request->dept_active
        ]);
    }
}
