<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KategoriPengaduan extends Model
{
    use HasFactory;
    protected $guarded = ['id','created_at','updated_at'];
    protected $table = 'kategori_pengaduan';

    static function storeKategoriPengaduan($request)
    {
        KategoriPengaduan::create([
            'kp_nama' => $request->kp_nama,
            'kp_active'  => $request->kp_active
        ]);
    }

    /*static function updateKategoriPengaduan($id, $request)
    {
        KategoriPengaduan::where('id', $id)->update([
            'kp_nama' => $request->kp_nama,
            'kp_active'  => $request->kp_active
        ]);
    }

    static function deleteKategoriPengaduan($id)
    {
        KategoriPengaduan::where('id', $id)->delete();
    }*/
}
