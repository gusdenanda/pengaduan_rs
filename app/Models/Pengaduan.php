<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Auth;

class Pengaduan extends Model
{
    use HasFactory;

    protected $guarded = ['id','created_at','updated_at'];
    protected $table = 'pengaduan';

    static function storePengaduan($request)
    {
        $id_user = auth()->user()->id;
        if($id_user!=""){
            Pengaduan::create([
                'peng_tgl' => $request->peng_tgl,
                'user_id' => $request->user_id,
                'peng_nama' => $request->peng_nama,
                'peng_hp' => $request->peng_hp,
                'peng_email' => $request->peng_email,
                'kp_id' => $request->kp_id,
                'peng_topik' => $request->peng_topik,
                'peng_isi_pengaduan' => $request->peng_isi_pengaduan,
                'peng_file' => $request->peng_file,
                'peng_identitas_pelapor' => $request->peng_identitas_pelapor,
                'peng_sifat_pengaduan' => $request->peng_sifat_pengaduan,
                'dept_id'  => $request->dept_id
            ]);
        }else{
            Pengaduan::create([
                'peng_tgl' => $request->peng_tgl,
                'peng_nama' => $request->peng_nama,
                'peng_hp' => $request->peng_hp,
                'peng_email' => $request->peng_email,
                'kp_id' => $request->kp_id,
                'peng_topik' => $request->peng_topik,
                'peng_isi_pengaduan' => $request->peng_isi_pengaduan,
                'peng_file' => $request->peng_file,
                'peng_identitas_pelapor' => $request->peng_identitas_pelapor,
                'peng_sifat_pengaduan' => $request->peng_sifat_pengaduan,
                'dept_id'  => $request->dept_id
            ]);
        }
    }
}
