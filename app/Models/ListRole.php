<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class ListRole extends Model
{
    use HasFactory;

    protected $fillable = ['role_id', 'user_id'];
    public $timestamps = false;

    static function getRole($id)
    {
        return ListRole::where('user_id', $id)->get();
    }

    static function storeListRole($id, $role)
    {
        ListRole::where('user_id', $id)->delete();
        foreach ($role as $key => $value) {
            ListRole::insert([
                'role_id' => $value,
                'user_id' => $id
            ]);

            if ($key == 0) {
                DB::table('role_user')->where('user_id', $id)->delete();
                DB::table('role_user')->insert([
                    'role_id' => $value,
                    'user_id' => $id
                ]);
            }
        }
    }

    static function storeListRoleRegister($id, $role)
    {
        ListRole::where('user_id', $id)->delete();
        
        ListRole::insert([
            'role_id' => $role,
            'user_id' => $id
        ]);
        
        DB::table('role_user')->insert([
            'role_id' => $role,
            'user_id' => $id
        ]);
    }

    static function storeListRoleAuthor($id, $role)
    {
        ListRole::insert([
            'role_id' => $role,
            'user_id' => $id
        ]);
    }
}
