<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Models\Role;
use Illuminate\Support\Facades\Hash;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'no_hp',
        'address',
        'photo',
        'dept_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }
    public function authorizeRoles($roles)
    {
        if (is_array($roles)) {
            return $this->hasAnyRole($roles) || 
                    abort(401, 'This action is unauthorized.');
        }
        return $this->hasRole($roles) || 
                abort(401, 'This action is unauthorized.');
    }
    /**
    * Check multiple roles
    * @param array $roles
    */
    public function hasAnyRole($roles)
    {
        return null !== $this->roles()->whereIn('name', $roles)->first();
    }
    /**
    * Check one role
    * @param string $role
    */
    public function hasRole($role)
    {
        return null !== $this->roles()->where('name', $role)->first();
    }

    static function getUser()
    {
        $user = User::all();
        $data = [];
        if ($user->isNotEmpty()) {
            foreach ($user as $key => $value) {
                $data[$key] = User::findOrFail($value->id);

                $data[$key]['roles'] = ListRole::where('user_id', $value->id)
                                                ->select('roles.id', 'roles.description')
                                                ->join('roles', 'list_roles.role_id', 'roles.id')
                                                ->get();
            }
       
          
        }
       
        return $data;
    }

    static function firstUserDataUser($id)
    {
        $user = User::whereId($id)->first();
        $user['roles'] = ListRole::getRole($id);
        return $user;
    }

    static function storeUser($request)
    {
        return User::create([
            'name'      => $request->name,
            'email'     => $request->email,
            'no_hp'     => $request->no_hp,
            'dept_id'     => $request->dept_id,
            'password'  => Hash::make($request->no_induk),
        ]);
    }
    static function storeUserRegister($name, $email, $password)
    {
        return User::create([
            'name'      => $name,
            'email'     => $email,
            'password'  => Hash::make($password),
        ]);
    }
    static function updateUser($id, $request)
    {
        if($request->password!=""){
            User::whereId($id)->update([
                'name'      => $request->name,
                'email'     => $request->email,
                'no_hp'     => $request->no_hp,
                'address'     => $request->address,
                'dept_id'     => $request->dept_id,
                'password'  => Hash::make($request->password),
            ]);
        }else{
            User::whereId($id)->update([
                'name'      => $request->name,
                'no_hp'     => $request->no_hp,
                'email'     => $request->email,
                'dept_id'     => $request->dept_id,
                'address'     => $request->address
            ]);
        }
    }
    static function deleteUser($id)
    {
        User::whereId($id)->delete();
    }

    static function getUserByRole($id)
    {
        return User::join('list_roles', 'users.id', 'list_roles.user_id')
                    ->select('users.id', 'users.name', 'users.no_induk')
                    ->where('list_roles.role_id', $id)
                    ->get();
    }
}
