<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StatusTindaklanjut extends Model
{
    use HasFactory;
    protected $guarded = ['id','created_at','updated_at'];
    protected $table = 'status_tindaklanjut';

    static function storeStatusTindaklanjut($request)
    {
        StatusTindaklanjut::create([
            'stl_nama' => $request->stl_nama,
            'stl_active'  => $request->stl_active
        ]);
    }
}
